package com.jiankangjin.appserver.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jiankangjin.appserver.vo.GetUserInfoVo;
import com.jiankangjin.httpclient.HttpClient;

public class CheckToken {
	private static final transient Log logger = LogFactory.getLog(CheckToken.class);

	/**
	 * 验证token
	 * @param token
	 * @return
	 * GetUserInfoVo
	 */
	public static GetUserInfoVo getUserInfo(String token) {
		GetUserInfoVo getUserInfoVo = new GetUserInfoVo();
		try {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("token", token);
			Map<String, Object> map = HttpClient.getInstance()
					.doPOSTgetHashMap("/accmanage/query_user_info", params);
			if (map == null) {
				getUserInfoVo.setResp_status("9999");
				getUserInfoVo.setMsg("帐户系统下载用户信息返回空");
				return getUserInfoVo;
			}
			String[] resTip = ResourceMap.getArray("A_query_user_info_"
					+ (String) map.get("status"));
			getUserInfoVo.setResp_status(resTip[0]);
			getUserInfoVo.setMsg(resTip[1]);
			getUserInfoVo.setToken((String) map.get("token"));
			getUserInfoVo.setUser_name((String) map.get("username"));
			getUserInfoVo.setUuid((String) map.get("userid"));
			getUserInfoVo.setUser_gender((String) map.get("gender"));
			getUserInfoVo.setUser_addr((String) map.get("address"));
			getUserInfoVo.setSign("appSPOS_getUserInfo_sign");
			return getUserInfoVo;
		} catch (Exception e) {
			logger.error(e);
			return getUserInfoVo;
		}
	}
}
