package com.jiankangjin.appserver.util;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jiankangjin.httpclient.PropertiesLoader;

/**
 */
public class ResourceMap {
	private static final transient Log log = LogFactory
			.getLog(ResourceMap.class);

	public static String get(String key) {
		return resourceMap.get(key);
	}
	public static String[] getArray(String key) {
		return resourceMap.get(key).split(",");
	}

	private static Map<String, String> resourceMap = new HashMap<>();

	static {
		InputStream inputStream = PropertiesLoader.class.getClassLoader()
				.getResourceAsStream("resouces.properties");
		Properties pops = new Properties();
		try {
			pops.load(inputStream);
			Set<Entry<Object, Object>> set = pops.entrySet();
			Iterator<Map.Entry<Object, Object>> it = set.iterator();
			String key = null;
			String value = null;
			while (it.hasNext()) {
				Entry<Object, Object> entry = it.next();
				key = String.valueOf(entry.getKey());
				value = String.valueOf(entry.getValue());
				key = key == null ? key : key.trim();
				value = value == null ? value : value.trim();
				// 将key-value放入map中
				resourceMap.put(key, value);
			}
		} catch (Exception e) {
			log.error("加载HttpClient.properties失败");
		}
	}

}
