package com.jiankangjin.appserver.vo;

public class ValidateUserCertiVo {
	/** 响应状态 */
	private String resp_status;
	/** 访问令牌 */
	private String token;
	/** 令牌有效期 */
	private String valid_time;
	/** 手势密码 */
	private String gesture_password;
	/** 用户姓名 */
	private String user_name;
	/** 用户头像地址 */
	private String user_logo;
	/** 用户角色编号 */
	private String user_role;
	/** 商户名称 */
	private String merchant_name;
	/** 商户图标地址 */
	private String merchant_logo;
	/** 上次登陆时间 */
	// 7月22日新增
	private String lastLoginTime;

	/** 签名 */
	private String sign;

	private String msg;

	/**
	 * ValidateUserCertiVo 构造方法
	 */
	public ValidateUserCertiVo() {

	}



	/**
	 * @param resp_status
	 * @param token
	 * @param valid_time
	 * @param gesture_password
	 * @param user_name
	 * @param user_logo
	 * @param user_role
	 * @param merchant_name
	 * @param merchant_logo
	 * @param lastLoginTime
	 * @param sign
	 * @param msg
	 */
	public ValidateUserCertiVo(String resp_status, String token,
			String valid_time, String gesture_password, String user_name,
			String user_logo, String user_role, String merchant_name,
			String merchant_logo, String lastLoginTime, String sign, String msg) {
		super();
		this.resp_status = resp_status;
		this.token = token;
		this.valid_time = valid_time;
		this.gesture_password = gesture_password;
		this.user_name = user_name;
		this.user_logo = user_logo;
		this.user_role = user_role;
		this.merchant_name = merchant_name;
		this.merchant_logo = merchant_logo;
		this.lastLoginTime = lastLoginTime;
		this.sign = sign;
		this.msg = msg;
	}


	/**  */
	public String getMsg() {
		return msg;
	}

	/**  */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/** get */
	public String getResp_status() {
		return resp_status;
	}

	/** set */
	public void setResp_status(String resp_status) {
		this.resp_status = resp_status;
	}

	/** get */
	public String getToken() {
		return token;
	}

	/** set */
	public void setToken(String token) {
		this.token = token;
	}

	/** get */
	public String getValid_time() {
		return valid_time;
	}

	/** set */
	public void setValid_time(String valid_time) {
		this.valid_time = valid_time;
	}

	/** get */
	public String getGesture_password() {
		return gesture_password;
	}

	/** set */
	public void setGesture_password(String gesture_password) {
		this.gesture_password = gesture_password;
	}

	/** get */
	public String getUser_name() {
		return user_name;
	}

	/** set */
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	/** get */
	public String getUser_logo() {
		return user_logo;
	}

	/** set */
	public void setUser_logo(String user_logo) {
		this.user_logo = user_logo;
	}

	/** get */
	public String getUser_role() {
		return user_role;
	}

	/** set */
	public void setUser_role(String user_role) {
		this.user_role = user_role;
	}

	/** get */
	public String getMerchant_name() {
		return merchant_name;
	}

	/** set */
	public void setMerchant_name(String merchant_name) {
		this.merchant_name = merchant_name;
	}

	/** get */
	public String getMerchant_logo() {
		return merchant_logo;
	}

	/** set */
	public void setMerchant_logo(String merchant_logo) {
		this.merchant_logo = merchant_logo;
	}

	/** get */
	public String getSign() {
		return sign;
	}

	/** set */
	public void setSign(String sign) {
		this.sign = sign;
	}

	/** get 上次登陆时间 */
	public String getLastLoginTime() {
		return lastLoginTime;
	}

	/** set 上次登陆时间 */
	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}
}
