package com.jiankangjin.appserver.vo;

public class UpdateValidateImgVo {
	/** 响应状态 */
	private String resp_status;
	/** 验证会话编号  */
	private String tmp_ssn;
	/** 验证码图片地址  */
	private String img_url;
	
	private String msg;
	
	/**
	 * UpdateValidateImgVo 构造方法 
	 */
	public UpdateValidateImgVo() {
		
	}



	/**
	 * @param resp_status
	 * @param tmp_ssn
	 * @param img_url
	 * @param msg
	 */
	public UpdateValidateImgVo(String resp_status, String tmp_ssn,
			String img_url, String msg) {
		super();
		this.resp_status = resp_status;
		this.tmp_ssn = tmp_ssn;
		this.img_url = img_url;
		this.msg = msg;
	}



	/**  */
	public String getMsg() {
		return msg;
	}


	/**  */
	public void setMsg(String msg) {
		this.msg = msg;
	}


	/** get  */
	public String getResp_status() {
		return resp_status;
	}

	/** set  */
	public void setResp_status(String resp_status) {
		this.resp_status = resp_status;
	}

	/** get  */
	public String getTmp_ssn() {
		return tmp_ssn;
	}

	/** set  */
	public void setTmp_ssn(String tmp_ssn) {
		this.tmp_ssn = tmp_ssn;
	}

	/** get  */
	public String getImg_url() {
		return img_url;
	}

	/** set  */
	public void setImg_url(String img_url) {
		this.img_url = img_url;
	}

}
