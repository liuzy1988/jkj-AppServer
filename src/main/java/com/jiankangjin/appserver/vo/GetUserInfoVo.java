package com.jiankangjin.appserver.vo;

public class GetUserInfoVo {
    /**
     * 用户唯一标识码
     */
    private String uuid;
	private String token;
	/** 响应状态 */
	private String resp_status;
	/** 用户姓名 */
	private String user_name;
	/** 用户性别 */
	private String user_gender;
	/** 用户联系地址 */
	private String user_addr;
	/** 签名 */
	private String sign;

	private String msg;

	/**
	 * GetUserInfoVo 构造方法
	 */
	public GetUserInfoVo() {

	}



	/**
	 * @param token
	 * @param resp_status
	 * @param user_name
	 * @param user_gender
	 * @param user_addr
	 * @param sign
	 * @param msg
	 */
	public GetUserInfoVo(String token, String resp_status, String user_name,
			String user_gender, String user_addr, String sign, String msg) {
		super();
		this.token = token;
		this.resp_status = resp_status;
		this.user_name = user_name;
		this.user_gender = user_gender;
		this.user_addr = user_addr;
		this.sign = sign;
		this.msg = msg;
	}



	/**  */
	public String getMsg() {
		return msg;
	}

	/**  */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/** get */
	public String getToken() {
		return token;
	}

	/** set */
	public void setToken(String token) {
		this.token = token;
	}

	/** get */
	public String getResp_status() {
		return resp_status;
	}

	/** set */
	public void setResp_status(String resp_status) {
		this.resp_status = resp_status;
	}

	/** get */
	public String getUser_name() {
		return user_name;
	}

	/** set */
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	/** get */
	public String getUser_gender() {
		return user_gender;
	}

	/** set */
	public void setUser_gender(String user_gender) {
		this.user_gender = user_gender;
	}

	/** get */
	public String getUser_addr() {
		return user_addr;
	}

	/** set */
	public void setUser_addr(String user_addr) {
		this.user_addr = user_addr;
	}

	/** get */
	public String getSign() {
		return sign;
	}

	/** set */
	public void setSign(String sign) {
		this.sign = sign;
	}

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
