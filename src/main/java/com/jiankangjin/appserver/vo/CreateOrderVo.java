package com.jiankangjin.appserver.vo;

public class CreateOrderVo {
	/** 响应状态 */
	private String resp_status;
	/** 订单编号 */
	private String order_id;
	/** 推荐渠道信息Channel */
	private Object channel;
	/** 签名 */
	private String sign;
	
	private String msg;
	
	public CreateOrderVo() {

	}


	/**
	 * @param resp_status
	 * @param order_id
	 * @param channel
	 * @param sign
	 * @param msg
	 */
	public CreateOrderVo(String resp_status, String order_id, Object channel,
			String sign, String msg) {
		super();
		this.resp_status = resp_status;
		this.order_id = order_id;
		this.channel = channel;
		this.sign = sign;
		this.msg = msg;
	}



	public String getMsg() {
		return msg;
	}


	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getChannel() {
		return channel;
	}

	public void setChannel(Object channel) {
		this.channel = channel;
	}

	/** get 响应状态 */
	public String getResp_status() {
		return resp_status;
	}

	/** set 响应状态 */
	public void setResp_status(String resp_status) {
		this.resp_status = resp_status;
	}

	/** get 订单编号 */
	public String getOrder_id() {
		return order_id;
	}

	/** set 订单编号 */
	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}

	/** get 签名 */
	public String getSign() {
		return sign;
	}

	/** set 签名 */
	public void setSign(String sign) {
		this.sign = sign;
	}

}
