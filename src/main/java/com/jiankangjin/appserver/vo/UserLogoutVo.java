package com.jiankangjin.appserver.vo;

public class UserLogoutVo {
	/** 响应状态 */
	private String resp_status;
	/** 签名 */
	private String sign;
	
	private String msg;

	/**
	 * UserLogoutVo 构造方法
	 */
	public UserLogoutVo() {

	}


	/**
	 * @param resp_status
	 * @param sign
	 * @param msg
	 */
	public UserLogoutVo(String resp_status, String sign, String msg) {
		super();
		this.resp_status = resp_status;
		this.sign = sign;
		this.msg = msg;
	}


	/**  */
	public String getMsg() {
		return msg;
	}

	/**  */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/** get */
	public String getResp_status() {
		return resp_status;
	}

	/** set */
	public void setResp_status(String resp_status) {
		this.resp_status = resp_status;
	}

	/** get */
	public String getSign() {
		return sign;
	}

	/** set */
	public void setSign(String sign) {
		this.sign = sign;
	}

}
