package com.jiankangjin.appserver.vo;

public class UpdPasswordVo {
	/** 响应状态 */
	private String resp_status;
	/** 签名 */
	private String sign;
	
	private String msg;

	/**
	 * UpdPasswordVo 构造方法
	 */
	public UpdPasswordVo() {

	}



	/**
	 * @param resp_status
	 * @param sign
	 * @param msg
	 */
	public UpdPasswordVo(String resp_status, String sign, String msg) {
		super();
		this.resp_status = resp_status;
		this.sign = sign;
		this.msg = msg;
	}



	/**  */
	public String getMsg() {
		return msg;
	}

	/**  */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/** get 响应状态 */
	public String getResp_status() {
		return resp_status;
	}

	/** set 响应状态 */
	public void setResp_status(String resp_status) {
		this.resp_status = resp_status;
	}

	/** get 签名 */
	public String getSign() {
		return sign;
	}

	/** set 签名 */
	public void setSign(String sign) {
		this.sign = sign;
	}

}
