package com.jiankangjin.appserver.vo;

public class UserLoginVo {
	/** 响应状态  */
	private String resp_status;
	/** 低权限访问令牌 */
	private String token_low;
	/** 签名  */
	private String sign;
	
	//7月22加
	private String tmpSsn;
	private String mobilePhone;
	private String downloadUrl;
	
	private String msg;
	
	/**
	 * UserLoginVo 构造方法 
	 */
	public UserLoginVo() {
		
	}



	/**
	 * @param resp_status
	 * @param token_low
	 * @param sign
	 * @param tmpSsn
	 * @param mobilePhone
	 * @param downloadUrl
	 * @param msg
	 */
	public UserLoginVo(String resp_status, String token_low, String sign,
			String tmpSsn, String mobilePhone, String downloadUrl, String msg) {
		super();
		this.resp_status = resp_status;
		this.token_low = token_low;
		this.sign = sign;
		this.tmpSsn = tmpSsn;
		this.mobilePhone = mobilePhone;
		this.downloadUrl = downloadUrl;
		this.msg = msg;
	}



	/**  */
	public String getMsg() {
		return msg;
	}

	/**  */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/** get  */
	public String getTmpSsn() {
		return tmpSsn;
	}



	/** set  */
	public void setTmpSsn(String tmpSsn) {
		this.tmpSsn = tmpSsn;
	}



	/** get  */
	public String getMobilePhone() {
		return mobilePhone;
	}



	/** set  */
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}



	/** get  */
	public String getDownloadUrl() {
		return downloadUrl;
	}



	/** set  */
	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}



	/** get  */
	public String getResp_status() {
		return resp_status;
	}

	/** set  */
	public void setResp_status(String resp_status) {
		this.resp_status = resp_status;
	}

	/** get  */
	public String getToken_low() {
		return token_low;
	}

	/** set  */
	public void setToken_low(String token_low) {
		this.token_low = token_low;
	}

	/** get  */
	public String getSign() {
		return sign;
	}

	/** set  */
	public void setSign(String sign) {
		this.sign = sign;
	}
	
	
}
