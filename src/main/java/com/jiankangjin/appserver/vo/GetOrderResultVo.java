package com.jiankangjin.appserver.vo;

public class GetOrderResultVo {
	/** 响应状态 */
	private String resp_status;	
	/** Trade 交易记录  */
	private Object trade;
	/** 显示条数 */
	private int trade_quantity;
	/** 签名 */
	private String sign;
	
	private String msg;

	public GetOrderResultVo() {
		
	}
	



	/**
	 * @param resp_status
	 * @param trade
	 * @param trade_quantity
	 * @param sign
	 * @param msg
	 */
	public GetOrderResultVo(String resp_status, Object trade,
			int trade_quantity, String sign, String msg) {
		super();
		this.resp_status = resp_status;
		this.trade = trade;
		this.trade_quantity = trade_quantity;
		this.sign = sign;
		this.msg = msg;
	}




	public String getResp_status() {
		return resp_status;
	}

	public void setResp_status(String resp_status) {
		this.resp_status = resp_status;
	}

	public Object getTrade() {
		return trade;
	}

	public void setTrade(Object trade) {
		this.trade = trade;
	}

	public int getTrade_quantity() {
		return trade_quantity;
	}

	public void setTrade_quantity(int trade_quantity) {
		this.trade_quantity = trade_quantity;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}