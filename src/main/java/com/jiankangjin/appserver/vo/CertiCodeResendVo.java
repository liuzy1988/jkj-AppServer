package com.jiankangjin.appserver.vo;

public class CertiCodeResendVo {
	/** 响应状态 */
	private String resp_status;
	/** 低权限访问令牌 */
	private String token_low;
	/** 签名 */
	private String sign;

	private String msg;

	/**
	 * CertiCodeResendVo 构造方法
	 */
	public CertiCodeResendVo() {

	}

	/**
	 * @param resp_status
	 * @param token_low
	 * @param sign
	 * @param msg
	 */
	public CertiCodeResendVo(String resp_status, String token_low, String sign,
			String msg) {
		super();
		this.resp_status = resp_status;
		this.token_low = token_low;
		this.sign = sign;
		this.msg = msg;
	}


	/**  */
	public String getMsg() {
		return msg;
	}

	/**  */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/** get 响应状态 */
	public String getResp_status() {
		return resp_status;
	}

	/** set 响应状态 */
	public void setResp_status(String resp_status) {
		this.resp_status = resp_status;
	}

	/** get 低权限访问令牌 */
	public String getToken_low() {
		return token_low;
	}

	/** set 低权限访问令牌 */
	public void setToken_low(String token_low) {
		this.token_low = token_low;
	}

	/** get 签名 */
	public String getSign() {
		return sign;
	}

	/** set 签名 */
	public void setSign(String sign) {
		this.sign = sign;
	}

}
