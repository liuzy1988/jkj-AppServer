package com.jiankangjin.appserver.vo;

/**
 * @author liuzy
 * @date 2014年10月30日 下午1:17:39
 */
public class UserRegisterVo {
	private String resp_status;
	private String msg;
	/**  */
	public String getResp_status() {
		return resp_status;
	}
	/**  */
	public void setResp_status(String resp_status) {
		this.resp_status = resp_status;
	}
	/**  */
	public String getMsg() {
		return msg;
	}
	/**  */
	public void setMsg(String msg) {
		this.msg = msg;
	}

}
