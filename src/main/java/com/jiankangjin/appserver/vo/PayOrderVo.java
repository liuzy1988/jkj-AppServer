package com.jiankangjin.appserver.vo;

public class PayOrderVo {
	/** 响应状态 */
	private String resp_status;
	/** 支付交易流水号 */
	private String sub_deal_ssn;
	/** 当前支付交易状态 */
	private String sub_deal_stauts;
	/** 签名 */
	private String sign;
	
	private String msg;

	public PayOrderVo() {
	}



	/**
	 * @param resp_status
	 * @param sub_deal_ssn
	 * @param sub_deal_stauts
	 * @param sign
	 * @param msg
	 */
	public PayOrderVo(String resp_status, String sub_deal_ssn,
			String sub_deal_stauts, String sign, String msg) {
		super();
		this.resp_status = resp_status;
		this.sub_deal_ssn = sub_deal_ssn;
		this.sub_deal_stauts = sub_deal_stauts;
		this.sign = sign;
		this.msg = msg;
	}



	/**  */
	public String getMsg() {
		return msg;
	}


	/**  */
	public void setMsg(String msg) {
		this.msg = msg;
	}


	/** get */
	public String getResp_status() {
		return resp_status;
	}

	/** set */
	public void setResp_status(String resp_status) {
		this.resp_status = resp_status;
	}

	/** get */
	public String getSub_deal_ssn() {
		return sub_deal_ssn;
	}

	/** set */
	public void setSub_deal_ssn(String sub_deal_ssn) {
		this.sub_deal_ssn = sub_deal_ssn;
	}

	/** get */
	public String getSub_deal_stauts() {
		return sub_deal_stauts;
	}

	/** set */
	public void setSub_deal_stauts(String sub_deal_stauts) {
		this.sub_deal_stauts = sub_deal_stauts;
	}

	/** get */
	public String getSign() {
		return sign;
	}

	/** set */
	public void setSign(String sign) {
		this.sign = sign;
	}

}
