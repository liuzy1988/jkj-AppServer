package com.jiankangjin.appserver.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 5. 用户账户相关接口
 * @author liuzy
 * @date 2014年11月27日 上午9:22:47
 */
@Controller
public class UserAccountController {

	/**
	 * 5.1. 用户绑定账户/卡
	 * @param token
	 * @param card_id
	 * @param card_type
	 * @param card_password
	 * @param sign
	 * @return
	 * Map<?,?>
	 */
	@RequestMapping(value="/bind_user_account")
	@ResponseBody
	public Map<?,?> bindUserAccount(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String card_id,
			@RequestParam(required = false) String card_type,
			@RequestParam(required = false) String user_name,
			@RequestParam(required = false) String id_card,
			@RequestParam(required = false) String phone_number,
			@RequestParam(required = false) String card_password,
			@RequestParam(required = false) String sign
			) {
		Map<String, String> result = new HashMap<>();
		result.put("resp_status", "0000");
		result.put("resp_msg", "passed");
		result.put("sign", "AppServer_UserAccountController_bindUserAccount_sign");
		return result;
	}
	
	/**
	 * 5.2. 用户已绑定卡列表查询
	 * @param token
	 * @param card_id
	 * @param card_type
	 * @param sign
	 * @return
	 * Map<?,?>
	 */
	@RequestMapping(value="/all_user_account")
	@ResponseBody
	public Map<?,?> allUserAccount(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String card_id,
			@RequestParam(required = false) String card_type,
			@RequestParam(required = false) String sign
			) {
		Map<String, String> result = new HashMap<>();
		result.put("cards", "[{\"card_id\":\"001\",\"card_type\":\"0001\"},{\"card_id\":\"002\",\"card_id\":\"0002\"}]");
		result.put("card_count", "2");
		result.put("resp_status", "0000");
		result.put("resp_msg", "passed");
		result.put("sign", "AppServer_UserAccountController_allUserAccount_sign");
		return result;
	}
	/**
	 * 5.3. 用户解绑账户/卡
	 * @param token
	 * @param card_id
	 * @param card_type
	 * @param card_password
	 * @param sign
	 * @return
	 * Map<?,?>
	 */
	@RequestMapping(value="/unbind_user_account")
	@ResponseBody
	public Map<?,?> unbindUserAccount(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String card_id,
			@RequestParam(required = false) String card_type,
			@RequestParam(required = false) String sign
			) {
		Map<String, String> result = new HashMap<>();
		result.put("resp_status", "0000");
		result.put("resp_msg", "passed");
		result.put("sign", "AppServer_UserAccountController_unbindUserAccount_sign");
		return result;
	}
	
	/**
	 * 5.4. 卡信息查询
	 * @param token
	 * @param card_id
	 * @param card_type
	 * @param sign
	 * @return
	 * Map<?,?>
	 */
	@RequestMapping(value="/get_account_info")
	@ResponseBody
	public Map<?,?> getAccountInfo(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String card_id,
			@RequestParam(required = false) String card_type,
			@RequestParam(required = false) String sign
			) {
		Map<String, String> result = new HashMap<>();
		result.put("card_id", "001");
		result.put("card_type", "0002");
		result.put("balance", "10000");
		result.put("today_count", "3");
		result.put("today_amt", "4000");
		result.put("resp_status", "0000");
		result.put("resp_status", "0000");
		result.put("resp_msg", "passed");
		result.put("sign", "AppServer_UserAccountController_getAccountInfo_sign");
		return result;
	}
	
	/**
	 * 5.5. 查询卡消费记录
	 * @param token
	 * @param sign
	 * @return
	 * Map<?,?>
	 */
	@RequestMapping(value="/get_account_bill")
	@ResponseBody
	public Map<?,?> getAccountBill(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String card_id,
			@RequestParam(required = false) String card_type,
			@RequestParam(required = false) String sign
			) {
		Map<String, String> result = new HashMap<>();
		result.put("resp_status", "0000");
		result.put("resp_msg", "passed");
		result.put("sign", "AppServer_UserAccountController_getAccountBill_sign");
		return result;
	}
	
	/**
	 * 5.6. 授权申请
	 * @param token
	 * @param sign
	 * @return
	 * Map<?,?>
	 */
	@RequestMapping(value="/apply_permission")
	@ResponseBody
	public Map<?,?> applyPermission(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String to_user_id,
			@RequestParam(required = false) String to_user_name,
			@RequestParam(required = false) String message,
			@RequestParam(required = false) String sign
			) {
		Map<String, String> result = new HashMap<>();
		result.put("resp_status", "0000");
		result.put("resp_msg", "passed");
		result.put("sign", "AppServer_UserAccountController_applyPermission_sign");
		return result;
	}
	
	/**
	 * 5.7. 授权批复
	 * @param token
	 * @param sign
	 * @return
	 * Map<?,?>
	 */
	@RequestMapping(value="/reply_applied_permission")
	@ResponseBody
	public Map<?,?> replyAppliedPermission(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String to_user_id,
			@RequestParam(required = false) String reply,
			@RequestParam(required = false) String sign
			) {
		Map<String, String> result = new HashMap<>();
		result.put("resp_status", "0000");
		result.put("resp_msg", "passed");
		result.put("sign", "AppServer_UserAccountController_replyAppliedPermission_sign");
		return result;
	}
	
	/**
	 * 5.8. 查询授权列表
	 * @param token
	 * @param sign
	 * @return
	 * Map<?,?>
	 */
	@RequestMapping(value="/all_user_permission")
	@ResponseBody
	public Map<?,?> allUserPermission(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String sign
			) {
		Map<String, String> result = new HashMap<>();
		result.put("resp_status", "0000");
		result.put("resp_msg", "passed");
		result.put("sign", "AppServer_UserAccountController_allUserPermission_sign");
		return result;
	}
}
