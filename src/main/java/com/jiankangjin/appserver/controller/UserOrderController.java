package com.jiankangjin.appserver.controller;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jiankangjin.appserver.services.UserOrderServices;
import com.jiankangjin.appserver.util.CheckToken;
import com.jiankangjin.appserver.util.StringUtil;
import com.jiankangjin.appserver.vo.CreateOrderVo;
import com.jiankangjin.appserver.vo.GetOrderResultVo;
import com.jiankangjin.appserver.vo.GetUserInfoVo;
import com.jiankangjin.appserver.vo.PayOrderVo;

/**
 * 6. 用户账单相关接口
 * @author liuzy
 * @date 2014年11月27日 上午9:23:33
 */
@Controller
public class UserOrderController {
	private static final transient Log logger = LogFactory.getLog(UserOrderController.class);
	
	@Autowired
	UserOrderServices userOrderServices;
	
	/**
	 * 6.1. 创建订单
	 * @param token
	 * @param sign
	 * @return
	 * Map<?,?>
	 */
	@RequestMapping(value = "/create_order", method = RequestMethod.POST)
	@ResponseBody
	public CreateOrderVo createOrder(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String version,
			@RequestParam(required = false) String merchant_id,
			@RequestParam(required = false) String merchant_order_id,
			@RequestParam(required = false) String total_fee,
			@RequestParam(required = false) String currency,
			@RequestParam(required = false) String content,
			@RequestParam(required = false) String detail,
			@RequestParam(required = false) String is_tip,
			@RequestParam(required = false) String buyer_name,
			@RequestParam(required = false) String buyer_cellphone,
			@RequestParam(required = false) String comment,
            @RequestParam(required = false) String order_total_fee,//订单总额-折扣金额=订单金额(订单表中的ordAmt)
            @RequestParam(required = false) String discount_fee,
            @RequestParam(required = false) String discount_comment,
			@RequestParam(required = false) String sign) {
		if (logger.isInfoEnabled()) {
			StringBuilder sb = new StringBuilder();
			sb.append("token=" + token);
			sb.append(" version=" + version);
			sb.append(" merchant_id=" + merchant_id);
			sb.append(" merchant_order_id=" + merchant_order_id);
			sb.append(" total_fee=" + total_fee);
			sb.append(" currency=" + currency);
			sb.append(" content=" + content);
			sb.append(" detail=" + detail);
			sb.append(" is_tip=" + is_tip);
			sb.append(" buyer_name=" + buyer_name);
			sb.append(" buyer_cellphone=" + buyer_cellphone);
			sb.append(" comment=" + comment);
			sb.append(" order_total_fee=" + order_total_fee);
			sb.append(" discount_fee=" + discount_fee);
			sb.append(" discount_comment=" + discount_comment);
			sb.append(" sign=" + sign);
			logger.info("创建订单接口:" + sb);
		}
		CreateOrderVo createOrderVo = new CreateOrderVo();
		createOrderVo.setResp_status("9999");
		if (StringUtil.isEmpty(token)) {
			createOrderVo.setMsg("token不能为空");
			return createOrderVo;
		}
		if (StringUtil.isEmpty(merchant_order_id)) {
			createOrderVo.setMsg("merchant_order_id不能为空");
			return createOrderVo;
		}
		Integer _total_fee = 1;
		if (StringUtil.isEmpty(total_fee)) {
			createOrderVo.setMsg("total_fee不能为空");
			return createOrderVo;
		} else {
			if (StringUtil.isNotNum(total_fee)) {
				createOrderVo.setMsg("total_fee必须是数字");
				return createOrderVo;
			} else {
				_total_fee = Integer.parseInt(total_fee);
				if (_total_fee <= 0) {
					createOrderVo.setMsg("total_fee必须大于0");
					return createOrderVo;
				}
			}
		}
		if (StringUtil.isEmpty(detail)) {
			createOrderVo.setMsg("detail不能为空");
			return createOrderVo;
		}

		if (StringUtil.isEmpty(buyer_cellphone)) {
			createOrderVo.setMsg("buyer_cellphone不能为空");
			return createOrderVo;
		}
		boolean _is_tip = true;

		if (StringUtil.isNotEmpty(is_tip)) {
			if (is_tip.toLowerCase().equals("false") || is_tip.equals("0")) {
				_is_tip = false;
			}
		}

		if (StringUtil.isEmpty(sign)) {
			createOrderVo.setMsg("sign不能为空");
			return createOrderVo;
		}
		
		if(StringUtil.isEmpty(merchant_id)){
			createOrderVo.setMsg("merchant_id商户号不能为空！");
			return createOrderVo;
		}
		GetUserInfoVo userInfo = CheckToken.getUserInfo(token);
		if (!"0000".equals(userInfo.getResp_status())) {
			createOrderVo.setMsg("token验证失败");
			return createOrderVo;
		}
		String user_id = userInfo.getUuid();
		String user_name = userInfo.getUser_name();
		return userOrderServices.createOrder(merchant_id, user_id, user_name,
				merchant_order_id, _total_fee, currency, content, detail,
				_is_tip, buyer_name, buyer_cellphone, comment, order_total_fee,
				discount_fee, discount_comment);
	}
	
	/**
	 * 6.2. 订单支付
	 * @param token
	 * @param sign
	 * @return
	 * Map<?,?>
	 */
	@RequestMapping(value = "/pay_order", method = RequestMethod.POST)
	@ResponseBody
	public PayOrderVo payOrder(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String version,
			@RequestParam(required = false) String merchant_id,
			@RequestParam(required = false) String local_pay_ssn,
			@RequestParam(required = false) String order_id,
			@RequestParam(required = false) String fee,
			@RequestParam(required = false) String currency,
			@RequestParam(required = false) String channel_id,
			@RequestParam(required = false) String buyer_token,
			@RequestParam(required = false) String buyer_voucher,
			@RequestParam(required = false) String channel_type,
			@RequestParam(required = false) String sign) {
		if (logger.isInfoEnabled()) {
			StringBuilder sb = new StringBuilder();
			sb.append("token=" + token);
			sb.append(" version=" + version);
			sb.append(" merchant_id=" + merchant_id);
			sb.append(" local_pay_ssn=" + local_pay_ssn);
			sb.append(" order_id=" + order_id);
			sb.append(" fee=" + fee);
			sb.append(" currency=" + currency);
			sb.append(" channel_id=" + channel_id);
			sb.append(" buyer_token=" + buyer_token);
			sb.append(" buyer_voucher=" + buyer_voucher);
			sb.append(" channel_type=" + channel_type);
			sb.append(" sign=" + sign);
			logger.info("订单支付接口：" + sb);
		}
		PayOrderVo payOrderVo = new PayOrderVo();
		payOrderVo.setResp_status("9999");
		if (StringUtil.isEmpty(token)) {
			payOrderVo.setMsg("token不能为空");
			return payOrderVo;
		}
		if (StringUtil.isEmpty(local_pay_ssn)) {
			payOrderVo.setMsg("local_pay_ssn不能为空");
			return payOrderVo;
		}
		if (StringUtil.isEmpty(order_id)) {
			payOrderVo.setMsg("order_id不能为空");
			return payOrderVo;
		}
		Integer _fee = 1;
		if (StringUtil.isEmpty(fee)) {
			payOrderVo.setMsg("fee不能为空");
			return payOrderVo;
		} else {
			if (StringUtil.isNotNum(fee)) {
				payOrderVo.setMsg("fee必须是数字");
				return payOrderVo;
			} else {
				_fee = Integer.parseInt(fee);
				if (_fee <= 0) {
					payOrderVo.setMsg("fee必须大于0");
					return payOrderVo;
				}
			}
		}
		if (StringUtil.isEmpty(channel_id)) {
			payOrderVo.setMsg("channel_id不能为空");
			return payOrderVo;
		}
		if (StringUtil.isEmpty(buyer_token)) {
			payOrderVo.setMsg("buyer_token不能为空");
			return payOrderVo;
		}
		if (StringUtil.isEmpty(buyer_voucher)) {
			payOrderVo.setMsg("buyer_voucher不能为空");
			return payOrderVo;
		}
		if (StringUtil.isEmpty(channel_type)) {
			payOrderVo.setMsg("channel_type不能为空");
			return payOrderVo;
		}
		if (StringUtil.isEmpty(sign)) {
			payOrderVo.setMsg("sign不能为空");
			return payOrderVo;
		}
		GetUserInfoVo userInfo = CheckToken.getUserInfo(token);
		if (!"0000".equals(userInfo.getResp_status())) {
			payOrderVo.setMsg("token验证失败");
			return payOrderVo;
		}
		String user_id = userInfo.getUuid();
		String user_name = userInfo.getUser_name();
		return userOrderServices.payOrder(token, user_id, user_name,
				local_pay_ssn, order_id, _fee, currency, channel_id,
				buyer_token, buyer_voucher, channel_type);
	}

	/**
	 * 6.3. 订单查询
	 * @param token
	 * @param sign
	 * @return
	 * Map<?,?>
	 */
	@RequestMapping(value="/search_order")
	@ResponseBody
	public Map<?,?> searchOrder(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String version,
			@RequestParam(required = false) String pay_user_id,
			@RequestParam(required = false) String daifu_user_id,
			@RequestParam(required = false) String order_id,
			@RequestParam(required = false) String order_stat,
			@RequestParam(required = false) String order_date_begin,
			@RequestParam(required = false) String order_date_end,
			@RequestParam(required = false) String refund_date_begin,
			@RequestParam(required = false) String refund_date_end,
			@RequestParam(required = false) String search_word,
			@RequestParam(required = false) String sign
			) {
		Map<String, String> result = new HashMap<>();
		result.put("resp_status", "0000");
		result.put("resp_msg", "passed");
		result.put("sign", "AppServer_UserOrderController_searchOrder_sign");
		return result;
	}
	
	/**
	 * 6.4. 订单详情查询
	 * @param token
	 * @param sign
	 * @return
	 * GetOrderResultVo
	 */
	@RequestMapping(value="/get_order_result")
	@ResponseBody
	public GetOrderResultVo getOrderResult(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String result_id,
			@RequestParam(required = false) String page,
			@RequestParam(required = false) String quantity,
			@RequestParam(required = false) String sign
			) {
		if (logger.isInfoEnabled()) {
			StringBuilder sb = new StringBuilder();
			sb.append("token=" + token);
			sb.append(" result_id=" + result_id);
			sb.append(" page=" + page);
			sb.append(" quantity=" + quantity);
			sb.append(" sign=" + sign);
			logger.info("交易查询结果查看接口：" + sb);
		}
		GetOrderResultVo getOrderResultVo = new GetOrderResultVo();
		getOrderResultVo.setResp_status("9999");
		if (StringUtil.isEmpty(token)) {
			getOrderResultVo.setMsg("token不能为空");
			return getOrderResultVo;
		}
		if (StringUtil.isEmpty(result_id)) {
			getOrderResultVo.setMsg("result_id不能为空");
			return getOrderResultVo;
		}
		Integer _page = 1;
		if (StringUtil.isNotEmpty(page)) {
			if (StringUtil.isNotNum(page)) {
				getOrderResultVo.setMsg("page必须为数字");
				return getOrderResultVo;
			} else {
				_page = Integer.parseInt(page);
				if (_page <= 0) {
					getOrderResultVo.setMsg("page必须大于零");
					return getOrderResultVo;
				}
			}
		}
		Integer _quantity = 10;
		if (StringUtil.isNotEmpty(quantity)) {
			if (StringUtil.isNotNum(quantity)) {
				getOrderResultVo.setMsg("quantity必须为数字");
				return getOrderResultVo;
			} else {
				_quantity = Integer.parseInt(quantity);
				if (_quantity <= 0) {
					getOrderResultVo.setMsg("quantity必须大于零");
					return getOrderResultVo;
				}
			}
		}
		if (StringUtil.isEmpty(sign)) {
			getOrderResultVo.setMsg("sign不能为空");
			return getOrderResultVo;
		}
		getOrderResultVo.setTrade(new Object());
		getOrderResultVo.setTrade_quantity(0);
		getOrderResultVo.setResp_status("0000");
		getOrderResultVo.setMsg("passed");
		getOrderResultVo.setSign("AppServer_UserOrderController_payOrder_sign");
		return getOrderResultVo;
	}
	
	/**
	 * 6.5 个人历月账单查询
	 * @param token
	 * @param sign
	 * @return
	 * Map<?,?>
	 */
	@RequestMapping(value="/search_all_month_bill")
	@ResponseBody
	public Map<?,?> searchAllMonthBill(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String date,
			@RequestParam(required = false) String sign
			) {
		Map<String, String> result = new HashMap<>();
		result.put("detail", "["
				+ "{\"day\":\"01\",\"amount\":1000,\"paid\":600,\"comercial\":300,\"medical\":100},"
				+ "{\"day\":\"12\",\"amount\":4000,\"paid\":3000,\"comercial\":500,\"medical\":500},"
				+ "{\"day\":\"23\",\"amount\":2000,\"paid\":1000,\"comercial\":600,\"medical\":400}"
				+ "]");
		result.put("resp_status", "0000");
		result.put("resp_msg", "passed");
		result.put("sign", "AppServer_UserOrderController_medicalRecords_sign");
		return result;
	}
	
	/**
	 * 6.6 个人总账单查询
	 * @param token
	 * @param sign
	 * @return
	 * Map<?,?>
	 */
	@RequestMapping(value="/search_bill")
	@ResponseBody
	public Map<?,?> searchBill(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String date,
			@RequestParam(required = false) String sign
			) {
		Map<String, String> result = new HashMap<>();
		result.put("user_id", "20141201103549517-000203EpaaaiSN");
		result.put("user_name", "老张");
		result.put("toal_paid", "440000");
		result.put("own_paid", "140000");
		result.put("own_comercial", "50000");
		result.put("own_medical", "50000");
		result.put("other_paid", "400000");
		result.put("other_detail", "["
				+ "{\"user_id\":\"20141118130244212-000019vJTsqQwe\",\"user_name\":\"张老爷\",\"paid\":150000,\"comercial\":50000,\"medical\":50000},"
				+ "{\"user_id\":\"20141128152823728-000017w26B35GL\",\"user_name\":\"张老太\",\"paid\":200000,\"comercial\":50000,\"medical\":50000},"
				+ "{\"user_id\":\"20141128162221064-000140cJZ31e0w\",\"user_name\":\"李婶\",\"paid\":50000,\"comercial\":50000,\"medical\":50000}"
				+ "]");
		result.put("resp_status", "0000");
		result.put("resp_msg", "passed");
		result.put("sign", "AppServer_UserOrderController_medicalRecords_sign");
		return result;
	}
	
	/**
	 * 6.7 个人对月账单查询
	 * @param token
	 * @param sign
	 * @return
	 * Map<?,?>
	 */
	@RequestMapping(value="/search_month_bill")
	@ResponseBody
	public Map<?,?> searchMonthBill(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String date,
			@RequestParam(required = false) String other_user_id,
			@RequestParam(required = false) String sign
			) {
		Map<String, String> result = new HashMap<>();
		result.put("paid", "150000");
		result.put("comercial", "50000");
		result.put("medical", "50000");
		result.put("result_id", "1354634815512");
		result.put("quantiy", "3");
		result.put("resp_status", "0000");
		result.put("resp_msg", "passed");
		result.put("sign", "AppServer_UserOrderController_medicalRecords_sign");
		return result;
	}
	
	/**
	 * 6.8 个人对月账单查询结果
	 * @param token
	 * @param sign
	 * @return
	 * Map<?,?>
	 */
	@RequestMapping(value="/get_month_bill_result")
	@ResponseBody
	public Map<?,?> getMonthBillResult(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String result_id,
			@RequestParam(required = false) String page,
			@RequestParam(required = false) String quantiy,
			@RequestParam(required = false) String other_user_id,
			@RequestParam(required = false) String sign
			) {
		Map<String, String> result = new HashMap<>();
		result.put("trade", "["
				+ "{\"orde_id\":\"20141120144756666-000107fL5VFHsT\",\"orde_merchant\",\"上海某药店\",\"pay_time\":\"20141120144756\",\"pay_amt\":100000,\"comercial\":50000,\"medical\":50000},"
				+ "{\"orde_id\":\"20141121165435056-000019xjZEx2vf\",\"orde_merchant\",\"上海某药店\",\"pay_time\":\"20141121165435\",\"pay_amt\":40000,\"comercial\":50000,\"medical\":50000},"
				+ "{\"orde_id\":\"20141201104917233-000005sN9195tY\",\"orde_merchant\",\"上海某药店\",\"pay_time\":\"20141201104917\",\"pay_amt\":10000,\"comercial\":50000,\"medical\":50000}"
				+ "]");
		result.put("quantiy", "3");
		result.put("resp_status", "0000");
		result.put("resp_msg", "passed");
		result.put("sign", "AppServer_UserOrderController_medicalRecords_sign");
		return result;
	}
}
