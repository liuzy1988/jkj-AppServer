package com.jiankangjin.appserver.controller;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jiankangjin.appserver.services.UserActionServices;
import com.jiankangjin.appserver.vo.CertiCodeResendVo;
import com.jiankangjin.appserver.vo.UpdateValidateImgVo;
import com.jiankangjin.appserver.vo.UserLoginVo;
import com.jiankangjin.appserver.vo.UserLogoutVo;
import com.jiankangjin.appserver.vo.UserRegisterVo;
import com.jiankangjin.appserver.vo.ValidateUserCertiVo;

/**
 * 3. 用户动作相关接口 注册登陆等
 * @author liuzy
 * @date 2014年11月27日 上午9:22:24
 */
@Controller
public class UserActionController {
	private static final transient Log logger = LogFactory.getLog(UserActionController.class);
	
	@Autowired
	UserActionServices userActionServices;
	
	/**
	 * 3.1. 校验账户是否存在
	 * @param token 调用接口凭证
	 * @param user_name 账号
	 * @param user_type 账号类型
	 * @param sign 签名
	 * @return
	 * Map<?,?>
	 */
	@RequestMapping(value="/check_user_exists")
	@ResponseBody
	public Map<?,?> checkUserExists(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String user_name,
			@RequestParam(required = false) String user_type,
			@RequestParam(required = false) String sign
			) {
		if (logger.isInfoEnabled()) {
			StringBuilder sb = new StringBuilder();
			sb.append("token=" + token);
			sb.append(" user_name=" + user_name);
			sb.append(" user_type=" + user_type);
			sb.append(" sign=" + sign);
			logger.info("校验账户是否存在接口：" + sb);
		}
		Map<String, String> result = new HashMap<>();
		result.put("user_name", user_name);
		result.put("user_type", user_type);
		result.put("resp_status", "0001");
		result.put("resp_msg", "no_exists");
		result.put("sign", "AppServer_UserActionController_checkUserExists_sign");
		return result;
	}
	
	/**
	 * 3.2. 用户注册
	 */
	@RequestMapping(value="/user_register", method=RequestMethod.POST)
	@ResponseBody
	public UserRegisterVo userRegister(
			@RequestParam(required = false) String tmp_ssn,
			@RequestParam(required = false) String username,
			@RequestParam(required = false) String password,
			@RequestParam(required = false) String sign) {
		if (logger.isInfoEnabled()) {
			StringBuilder sb = new StringBuilder();
			sb.append("tmp_ssn=" + tmp_ssn);
			sb.append(" username=" + username);
			sb.append(" password=" + password);
			sb.append(" sign=" + sign);
			logger.info("用户注册接口:" + sb);
		}
		UserRegisterVo userRegisterVo = new UserRegisterVo();
		userRegisterVo.setResp_status("9999");
		if (tmp_ssn == null || tmp_ssn.equals("")) {
			userRegisterVo.setMsg("tmp_ssn不能为空");
			return userRegisterVo;
		}
		if (username == null || username.equals("")) {
			userRegisterVo.setMsg("username不能为空");
			return userRegisterVo;
		}
		if (password == null || password.equals("")) {
			userRegisterVo.setMsg("password不能为空");
			return userRegisterVo;
		}
		if (sign == null || sign.equals("")) {
			userRegisterVo.setMsg("sign不能为空");
			return userRegisterVo;
		}
		return userActionServices.userRegister(tmp_ssn, username, password);
	}
	
	/**
	 * 3.3. 更新验证码
	 */
	@RequestMapping(value="/update_validate_img", method=RequestMethod.POST)
	@ResponseBody
	public UpdateValidateImgVo updateValidateImg(){
		logger.info("更新验码接口");
		return userActionServices.updateValidateImg();
	}
	
	
	/**
	 * 3.4. 用户登陆
	 */
	@RequestMapping(value="/user_login", method=RequestMethod.POST)
	@ResponseBody
	public UserLoginVo userLogin(
			@RequestParam(required = false) String tmp_ssn,
			@RequestParam(required = false) String validate_code,
			@RequestParam(required = false) String login_voucher,
			@RequestParam(required = false) String login_password,
			@RequestParam(required = false) String terminal_type,
			@RequestParam(required = false) String terminal_ver,
			@RequestParam(required = false) String terminal_id,
			@RequestParam(required = false) String sign) {
		if (logger.isInfoEnabled()) {
			StringBuilder sb = new StringBuilder();
			sb.append("tmp_ssn=" + tmp_ssn);
			sb.append(" validate_code=" + validate_code);
			sb.append(" login_voucher=" + login_voucher);
			sb.append(" login_password=" + login_password);
			sb.append(" terminal_type=" + terminal_type);
			sb.append(" terminal_ver=" + terminal_ver);
			sb.append(" terminal_id=" + terminal_id);
			sb.append(" sign=" + sign);
			logger.info("用户登陆接口:" + sb);
		}
		UserLoginVo userLoginVo = new UserLoginVo();
		userLoginVo.setResp_status("9999");
		if (tmp_ssn == null || tmp_ssn.equals("")) {
			userLoginVo.setMsg("tmp_ssn不能为空");
			return userLoginVo;
		}
		if (validate_code == null || validate_code.equals("")) {
			userLoginVo.setMsg("validate_code不能为空");
			return userLoginVo;
		}
		if (login_voucher == null || login_voucher.equals("")) {
			userLoginVo.setMsg("login_voucher不能为空");
			return userLoginVo;
		}
		if (login_password == null || login_password.equals("")) {
			userLoginVo.setMsg("login_password不能为空");
			return userLoginVo;
		}
		if (terminal_type == null || terminal_type.equals("")) {
			userLoginVo.setMsg("terminal_type不能为空");
			return userLoginVo;
		}
		if (terminal_ver == null || terminal_ver.equals("")) {
			userLoginVo.setMsg("terminal_ver不能为空");
			return userLoginVo;
		}
		if (terminal_id == null || terminal_id.equals("")) {
			userLoginVo.setMsg("terminal_id不能为空");
			return userLoginVo;
		}
		if (sign == null || sign.equals("")) {
			userLoginVo.setMsg("sign不能为空");
			return userLoginVo;
		}
		return userActionServices.userLogin(tmp_ssn, validate_code, login_voucher, login_password, terminal_type, terminal_ver, terminal_id);
	}
	
	/**
	 * 3.5. 身份校验凭证重发
	 */
	@RequestMapping(value="/certi_code_resend", method=RequestMethod.POST)
	@ResponseBody
	public CertiCodeResendVo certiCodeResend(
			@RequestParam(required = false) String token_low,
			@RequestParam(required = false) String sign){
		if (logger.isInfoEnabled()) {
			StringBuilder sb = new StringBuilder();
			sb.append("token_low=" + token_low);
			sb.append(" sign=" + sign);
			logger.info("身份校验凭证重发接口:" + sb);
		}
		CertiCodeResendVo certiCodeResendVo = new CertiCodeResendVo();
		certiCodeResendVo.setResp_status("9999");
		if (token_low == null || token_low.equals("")) {
			certiCodeResendVo.setMsg("token_low不能为空");
			return certiCodeResendVo;
		}
		if (sign == null || sign.equals("")) {
			certiCodeResendVo.setMsg("sign不能为空");
			return certiCodeResendVo;
		}
		return userActionServices.certiCodeResend(token_low);
	}
	
	/**
	 * 3.6. 身份校验(短信码)请求 
	 */
	@RequestMapping(value="/validate_user_certi", method=RequestMethod.POST)
	@ResponseBody
	public ValidateUserCertiVo validateUserCerti(
			@RequestParam(required = false) String token_low,
			@RequestParam(required = false) String certi_code,
			@RequestParam(required = false) String sign){
		if (logger.isInfoEnabled()) {
			StringBuilder sb = new StringBuilder();
			sb.append("token_low=" + token_low);
			sb.append(" certi_code=" + certi_code);
			sb.append(" sign=" + sign);
			logger.info("身份校验(短信码)请求接口:" + sb);
		}
		ValidateUserCertiVo validateUserCertiVo = new ValidateUserCertiVo();
		validateUserCertiVo.setResp_status("9999");
		if (token_low == null || token_low.equals("")) {
			validateUserCertiVo.setMsg("token_low不能为空");
			return validateUserCertiVo;
		}
		if (certi_code == null || certi_code.equals("")) {
			validateUserCertiVo.setMsg("certi_code不能为空");
			return validateUserCertiVo;
		}
		if (sign == null || sign.equals("")) {
			validateUserCertiVo.setMsg("sign不能为空");
			return validateUserCertiVo;
		}
		return userActionServices.validateUserCerti(token_low, certi_code);
	}
	
	
	/**
	 * 3.7. 用户注销登陆
	 */
	@RequestMapping(value="/user_logout", method=RequestMethod.POST)
	@ResponseBody
	public UserLogoutVo userLogout(
			@RequestParam(required = false) String token,
			@RequestParam(required=false) String sign) {
		if (logger.isInfoEnabled()) {
			StringBuilder sb = new StringBuilder();
			sb.append("token=" + token);
			sb.append(" sign=" + sign);
			logger.info("登陆注销接口:" + sb);
		}
		UserLogoutVo userLogoutVo = new UserLogoutVo();
		userLogoutVo.setResp_status("9999");
		if (token == null || token.equals("")) {
			userLogoutVo.setMsg("token不能为空");
			return userLogoutVo;
		}
		if (sign == null || sign.equals("")) {
			userLogoutVo.setMsg("sign不能为空");
			return userLogoutVo;
		}
		return userActionServices.userLogout(token);
	}
}
