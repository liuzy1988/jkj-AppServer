package com.jiankangjin.appserver.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 7. 其他功能接口
 * @author liuzy
 * @date 2014年11月27日 上午9:24:35
 */
@Controller
public class OtherController {

	/**
	 * 7.1. 资讯列表查询
	 * @param token
	 * @param sign
	 * @return
	 * Map<?,?>
	 */
	@RequestMapping(value="/search_news")
	@ResponseBody
	public Map<?,?> searchNews(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String sign
			) {
		Map<String, String> result = new HashMap<>();
		result.put("resp_status", "0000");
		result.put("resp_msg", "passed");
		result.put("sign", "AppServer_OtherController_searchNews_sign");
		return result;
	}
	/**
	 * 7.2. 资讯详情查询
	 * @param token
	 * @param sign
	 * @return
	 * Map<?,?>
	 */
	@RequestMapping(value="/get_news_result")
	@ResponseBody
	public Map<?,?> getNewsResult(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String sign
			) {
		Map<String, String> result = new HashMap<>();
		result.put("resp_status", "0000");
		result.put("resp_msg", "passed");
		result.put("sign", "AppServer_OtherController_getNewsResult_sign");
		return result;
	}
	/**
	 * 7.3. 附近商户
	 * @param token
	 * @param sign
	 * @return
	 * Map<?,?>
	 */
	@RequestMapping(value="/near_merchants")
	@ResponseBody
	public Map<?,?> nearMerchants(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String sign
			) {
		Map<String, String> result = new HashMap<>();
		result.put("resp_status", "0000");
		result.put("resp_msg", "passed");
		result.put("sign", "AppServer_OtherController_near_merchants_sign");
		return result;
	}
	/**
	 * 7.4. 收藏商户
	 * @param token
	 * @param sign
	 * @return
	 * Map<?,?>
	 */
	@RequestMapping(value="/favorites_merchants")
	@ResponseBody
	public Map<?,?> favoritesMerchants(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String sign
			) {
		Map<String, String> result = new HashMap<>();
		result.put("resp_status", "0000");
		result.put("resp_msg", "passed");
		result.put("sign", "AppServer_OtherController_favoritesMerchants_sign");
		return result;
	}
	/**
	 * 7.5. 检查更新
	 * @param token
	 * @param sign
	 * @return
	 * Map<?,?>
	 */
	@RequestMapping(value="/check_update")
	@ResponseBody
	public Map<?,?> checkUpdate(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String sign
			) {
		Map<String, String> result = new HashMap<>();
		result.put("resp_status", "0000");
		result.put("resp_msg", "passed");
		result.put("sign", "AppServer_OtherController_checkUpdate_sign");
		return result;
	}
	/**
	 * 7.6. 提交反馈意见
	 * @param token
	 * @param sign
	 * @return
	 * Map<?,?>
	 */
	@RequestMapping(value="/submit_suggestions")
	@ResponseBody
	public Map<?,?> submitSuggestions(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String sign
			) {
		Map<String, String> result = new HashMap<>();
		result.put("resp_status", "0000");
		result.put("resp_msg", "passed");
		result.put("sign", "AppServer_OtherController_submitSuggestions_sign");
		return result;
	}
	/**
	 * 7.7. 查询反馈意见列表
	 * @param token
	 * @param sign
	 * @return
	 * Map<?,?>
	 */
	@RequestMapping(value="/search_suggestions")
	@ResponseBody
	public Map<?,?> searchSuggestions(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String sign
			) {
		Map<String, String> result = new HashMap<>();
		result.put("resp_status", "0000");
		result.put("resp_msg", "passed");
		result.put("sign", "AppServer_OtherController_searchSuggestions_sign");
		return result;
	}
	/**
	 * 7.8. 查询反馈意见详情
	 * @param token
	 * @param sign
	 * @return
	 * Map<?,?>
	 */
	@RequestMapping(value="/get_suggestions_result")
	@ResponseBody
	public Map<?,?> getSuggestionsResult(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String sign
			) {
		Map<String, String> result = new HashMap<>();
		result.put("resp_status", "0000");
		result.put("resp_msg", "passed");
		result.put("sign", "AppServer_OtherController_getSuggestionsResult_sign");
		return result;
	}
}
