package com.jiankangjin.appserver.controller;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jiankangjin.appserver.services.UserInfoServerices;
import com.jiankangjin.appserver.vo.GetUserInfoVo;
import com.jiankangjin.appserver.vo.UpdPasswordVo;

/**
 * 4. 用户信息相关接口
 * @author liuzy
 * @date 2014年11月27日 上午9:23:06
 */
@Controller
public class UserInfoController {
	private static final transient Log logger = LogFactory.getLog(UserInfoController.class);

	@Autowired
	UserInfoServerices userInfoServerices;

	/**
	 * 4.1. 实名认证
	 * @param token 用户凭证
	 * @param card_number 银行卡卡号
	 * @param card_type 卡类型
	 * @param exp_date 有效期
	 * @param name 开户名
	 * @param cer_number 证件号
	 * @param cer_type 证件类型
	 * @param cvv2 CVV2
	 * @param phone_number 预留手机号
	 * @param password 卡查询密码
	 * @param sign 签名
	 * @return
	 * Map<?,?>
	 */
	@RequestMapping(value="/validate_real_name", method=RequestMethod.POST)
	@ResponseBody
	public Map<?,?> validateRealName(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String card_number,
			@RequestParam(required = false) String card_type,
			@RequestParam(required = false) String exp_date,
			@RequestParam(required = false) String name,
			@RequestParam(required = false) String cer_number,
			@RequestParam(required = false) String cer_type,
			@RequestParam(required = false) String cvv2,
			@RequestParam(required = false) String phone_number,
			@RequestParam(required = false) String password,
			@RequestParam(required = false) String sign
			) {
		Map<String, String> result = new HashMap<>();
		result.put("resp_status", "0001");
		result.put("resp_msg", "yes");
		result.put("sign", "AppServer_UserInfoController_validateRealName_sign");
		return result;
	}
	
	/**
	 * 4.2. 获取实名认证协议
	 * @param token
	 * @param agreement_id
	 * @param agreement_type
	 * @param sign
	 * @return
	 * Map<?,?>
	 */
	@RequestMapping(value="/get_real_name_agreement", method=RequestMethod.POST)
	@ResponseBody
	public Map<?,?> getRealNameAgreement(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String agreement_id,
			@RequestParam(required = false) String agreement_type,
			@RequestParam(required = false) String sign
			) {
		Map<String, String> result = new HashMap<>();
		result.put("body", "0001");
		result.put("resp_status", "0001");
		result.put("resp_msg", "yes");
		result.put("sign", "AppServer_UserInfoController_getRealNameAgreement_sign");
		return result;
	}
	/**
	 * 4.3. 验证实名信息
	 */
	@RequestMapping(value="/validate_real_name_info", method=RequestMethod.POST)
	@ResponseBody
	public Map<?,?> validateRealNameInfo(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String user_name,
			@RequestParam(required = false) String user_type,
			@RequestParam(required = false) String answer1,
			@RequestParam(required = false) String answer2,
			@RequestParam(required = false) String answer3,
			@RequestParam(required = false) String sign
			) {
		Map<String, String> result = new HashMap<>();
		result.put("body", "0001");
		result.put("resp_status", "0001");
		result.put("resp_msg", "yes");
		result.put("sign", "AppServer_UserInfoController_validateRealNameInfo_sign");
		return result;
	}

	/**
	 * 4.4. 设置用户密保问题
	 * @return
	 * Map<?,?>
	 */
	@RequestMapping(value="/upd_security_question", method=RequestMethod.POST)
	@ResponseBody
	public Map<?,?> updSecurityQuestion(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String question1,
			@RequestParam(required = false) String answer1,
			@RequestParam(required = false) String question2,
			@RequestParam(required = false) String answer2,
			@RequestParam(required = false) String question3,
			@RequestParam(required = false) String answer3,
			@RequestParam(required = false) String sign
			) {
		Map<String, String> result = new HashMap<>();
		result.put("resp_status", "0001");
		result.put("resp_msg", "yes");
		result.put("sign", "AppServer_UserInfoController_updSecurityQuestion_sign");
		return result;
	}

	/**
	 * 4.5. 设置用户头像
	 * @return
	 * Map<?,?>
	 */
	@RequestMapping(value="/upd_user_head_photo", method=RequestMethod.POST)
	@ResponseBody
	public Map<?,?> updUserHeadPhoto(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String image,
			@RequestParam(required = false) String sign
			) {
		Map<String, String> result = new HashMap<>();
		result.put("resp_status", "0001");
		result.put("resp_msg", "yes");
		result.put("sign", "AppServer_UserInfoController_updUserHeadPhoto_sign");
		return result;
	}
	
	/**
	 * 4.6. 下载用户信息
	 */
	@RequestMapping(value="/get_user_info", method=RequestMethod.POST)
	@ResponseBody
	public GetUserInfoVo getUserInfo(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String sign
			){
		StringBuilder sb = new StringBuilder();
		sb.append("token=" + token);
		sb.append(" sign=" + sign);
		if (logger.isInfoEnabled()) {
			logger.info("下载用户信息接口:" + sb);
		}
		GetUserInfoVo getUserInfoVo = new GetUserInfoVo();
		getUserInfoVo.setResp_status("9999");
		if (token == null || token.equals("")) {
			getUserInfoVo.setMsg("token不能为空");
			return getUserInfoVo;
		}
		if (sign == null || sign.equals("")) {
			getUserInfoVo.setMsg("sign不能为空");
			return getUserInfoVo;
		}
		return userInfoServerices.getUserInfo(token);
	}
	
	
	/**
	 * 4.7. 修改登陆密码
	 */
	@RequestMapping(value="/upd_password", method=RequestMethod.POST)
	@ResponseBody
	public UpdPasswordVo updPassword(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String old_password,
			@RequestParam(required = false) String new_password,
			@RequestParam(required = false) String sign){
		StringBuilder sb = new StringBuilder();
		sb.append("token=" + token);
		sb.append(" old_password=" + old_password);
		sb.append(" new_password=" + new_password);
		sb.append(" sign=" + sign);
		if (logger.isInfoEnabled()) {
			logger.info("密码更新接口:" + sb);
		}
		UpdPasswordVo updPasswordVo = new UpdPasswordVo();
		updPasswordVo.setResp_status("9999");
		if (token == null || token.equals("")) {
			updPasswordVo.setMsg("token不能为空");
			return updPasswordVo;
		}
		if (old_password == null || old_password.equals("")) {
			updPasswordVo.setMsg("old_password不能为空");
			return updPasswordVo;
		}
		if (new_password == null || new_password.equals("")) {
			updPasswordVo.setMsg("new_password不能为空");
			return updPasswordVo;
		}
		if (sign == null || sign.equals("")) {
			updPasswordVo.setMsg("sign不能为空");
			return updPasswordVo;
		}
		return userInfoServerices.updPassword(token, old_password, new_password);
	}

	/**
	 * 4.8. 修改支付密码
	 * @return
	 * Map<?,?>
	 */
	@RequestMapping(value="/upd_pay_password", method=RequestMethod.POST)
	@ResponseBody
	public Map<?,?> updPayPassword(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String old_pay_password,
			@RequestParam(required = false) String new_pay_passwod,
			@RequestParam(required = false) String sign
			) {
		Map<String, String> result = new HashMap<>();
		result.put("resp_status", "0001");
		result.put("resp_msg", "yes");
		result.put("sign", "AppServer_UserInfoController_updPayPassword_sign");
		return result;
	}
	
	/**
	 * 4.9. 验证支付密码
	 * @return
	 * Map<?,?>
	 */
	@RequestMapping(value="/validate_pay_password", method=RequestMethod.POST)
	@ResponseBody
	public Map<?,?> validatePayPassword(
			@RequestParam(required = false) String token,
			@RequestParam(required = false) String pay_password,
			@RequestParam(required = false) String sign
			) {
		Map<String, String> result = new HashMap<>();
		result.put("resp_status", "0001");
		result.put("resp_msg", "yes");
		result.put("sign", "AppServer_UserInfoController_updPayPassword_sign");
		return result;
	}
}
