package com.jiankangjin.appserver.services;

import com.jiankangjin.appserver.vo.CertiCodeResendVo;
import com.jiankangjin.appserver.vo.UpdateValidateImgVo;
import com.jiankangjin.appserver.vo.UserLoginVo;
import com.jiankangjin.appserver.vo.UserLogoutVo;
import com.jiankangjin.appserver.vo.UserRegisterVo;
import com.jiankangjin.appserver.vo.ValidateUserCertiVo;

public interface UserActionServices {

	/**
	 * 更新验证码
	 * 
	 * @return UpdateValidateImgVo
	 */
	public UpdateValidateImgVo updateValidateImg();

	/**
	 * 用户注册
	 * 
	 * @param tmp_ssn
	 * @param username
	 * @param password
	 * @return UserRegisterVo
	 */
	public UserRegisterVo userRegister(String tmp_ssn, String username,
			String password);

	/**
	 * 用户登录
	 * 
	 * @param tmpSsn
	 * @param validateCode
	 * @param loginVouche
	 * @param loginPassword
	 * @param terminalType
	 * @param terminalVer
	 * @param terminalId
	 * @return UserLoginVo
	 */
	public UserLoginVo userLogin(String tmpSsn, String validateCode,
			String loginVouche, String loginPassword, String terminalType,
			String terminalVer, String terminalId);

	/**
	 * 登出
	 * 
	 * @param token
	 * @return UserLogoutVo
	 */
	public UserLogoutVo userLogout(String token);

	/**
	 * 身份校验凭证重发
	 * 
	 * @param tokenLow
	 * @return CertiCodeResendVo
	 */
	public CertiCodeResendVo certiCodeResend(String tokenLow);

	/**
	 * 身份校验请求
	 * 
	 * @param tokenLow
	 * @param certiCode
	 * @return ValidateUserCertiVo
	 */
	public ValidateUserCertiVo validateUserCerti(String tokenLow,
			String certiCode);

}
