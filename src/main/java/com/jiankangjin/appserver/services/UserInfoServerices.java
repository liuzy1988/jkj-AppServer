package com.jiankangjin.appserver.services;

import com.jiankangjin.appserver.vo.GetUserInfoVo;
import com.jiankangjin.appserver.vo.UpdPasswordVo;

/**
 * @author liuzy
 * @date 2014年11月27日 上午10:37:27
 */
public interface UserInfoServerices {

	/**
	 * 密码更新
	 * 
	 * @param token
	 * @param oldPassword
	 * @param newPassword
	 * @return UpdPasswordVo
	 */
	public UpdPasswordVo updPassword(String token, String oldPassword,
			String newPassword);

	/**
	 * 下载用户信息
	 * 
	 * @param token
	 * @return GetUserInfoVo
	 */
	public GetUserInfoVo getUserInfo(String token);
}
