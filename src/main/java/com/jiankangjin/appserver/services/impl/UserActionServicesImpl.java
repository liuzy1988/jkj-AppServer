package com.jiankangjin.appserver.services.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.jiankangjin.appserver.services.UserActionServices;
import com.jiankangjin.appserver.util.ResourceMap;
import com.jiankangjin.appserver.vo.CertiCodeResendVo;
import com.jiankangjin.appserver.vo.UpdateValidateImgVo;
import com.jiankangjin.appserver.vo.UserLoginVo;
import com.jiankangjin.appserver.vo.UserLogoutVo;
import com.jiankangjin.appserver.vo.UserRegisterVo;
import com.jiankangjin.appserver.vo.ValidateUserCertiVo;
import com.jiankangjin.httpclient.HttpClient;

/**  
 * 用户登录<br>
 * 第一步：获取图片验证码<br>
 * 第二步：帐号，密码，验证码登陆<br>
 * 第三步：发送短信码<br>
 * 第四步：验证短信码 <br>
 */
@Service
public class UserActionServicesImpl implements UserActionServices{
	private static final transient Log logger = LogFactory.getLog(UserActionServicesImpl.class);
	
	/**
	 * 更新验证码
	 */
	@Override
	public UpdateValidateImgVo updateValidateImg() {
		UpdateValidateImgVo updateValidateImgVo = new UpdateValidateImgVo();
		try {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("tmpSsn", null);
			params.put("imgWidth", "100");
			params.put("imgHeight", "50");
			if (logger.isInfoEnabled()) {
				logger.info("调帐户系统更新验证码:"+params);
			}
			Map<String, Object> map = HttpClient.getInstance().doPOSTgetHashMap("/accmanage/update_validate_img", params);
			if (logger.isInfoEnabled()) {
				logger.info("帐户系统更新验证码返回:"+map);
			}
			if(map == null) {
				updateValidateImgVo.setResp_status("9999");
				updateValidateImgVo.setMsg("帐户系统更新验证码返回空");
				return updateValidateImgVo;
			}
			String[] resTip = ResourceMap.getArray("A_update_validate_img_" + (String) map.get("status"));
			updateValidateImgVo.setResp_status(resTip[0]);
			updateValidateImgVo.setMsg(resTip[1]);
			updateValidateImgVo.setTmp_ssn((String) map.get("tmpSsn"));
			updateValidateImgVo.setImg_url((String) map.get("imgUrl"));
			return updateValidateImgVo;
		} catch (Exception e) {
			logger.error(this, e);
			return updateValidateImgVo;
		}
	}

	@Override
	public UserRegisterVo userRegister(String tmp_ssn, String username, String password) {
		UserRegisterVo userRegisterVo = new UserRegisterVo();
		Map<String, String> params = new HashMap<>();
		params.put("tmpSsn", tmp_ssn);
		params.put("username", username);
		params.put("password", password);
		if (logger.isInfoEnabled()) {
			logger.info("调帐户系统用户注册:" + params);
		}
		Map<String, String> resultMap = HttpClient.getInstance().doPost("/accmanage/user_register", params);
		if (logger.isInfoEnabled()) {
			logger.info("帐户系统用户注册返回:" + resultMap);
		}
		if (resultMap == null) {
			userRegisterVo.setResp_status("9999");
			userRegisterVo.setMsg("帐户系统用户登陆返回空");
			return userRegisterVo;
		}
		return userRegisterVo;
	}
	
	/**
	 * 获得图片验证码后	用户登录
	 */
	@Override
	public UserLoginVo userLogin(String tmpSsn, String validateCode,
			String loginVouche, String loginPassword, String terminalType,
			String terminalVer, String terminalId) {
		UserLoginVo userLoginVo = new UserLoginVo();
		try {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("tmpSsn", tmpSsn);
			params.put("username", loginVouche);
			params.put("password", loginPassword);
			params.put("imgCode", validateCode);
			params.put("terminalType", terminalType);
			params.put("terminalVersion", terminalVer);
			params.put("terminalId", terminalId);
			params.put("terminalIP", "appSPOS_value");
			params.put("appId", "appSPOS_value");
			params.put("sign", "appSPOS_userLogin_sign");
			if (logger.isInfoEnabled()) {
				logger.info("调帐户系统用户登陆:" + params);
			}
			Map<String, Object> map = HttpClient.getInstance()
					.doPOSTgetHashMap("/accmanage/validate_user_info", params);
			if (logger.isInfoEnabled()) {
				logger.info("帐户系统用户登陆返回:" + map);
			}
			if (map == null) {
				userLoginVo.setResp_status("9999");
				userLoginVo.setMsg("帐户系统用户登陆返回空");
				return userLoginVo;
			}
			String[] resTip = ResourceMap.getArray("A_validate_user_info_" + (String) map.get("status"));
			userLoginVo.setResp_status(resTip[0]);
			userLoginVo.setMsg(resTip[1]);
			userLoginVo.setToken_low((String) map.get("tmpToken"));
			userLoginVo.setTmpSsn((String) map.get("tmpSsn"));
			userLoginVo.setMobilePhone((String) map.get("mobilePhone"));
			userLoginVo.setDownloadUrl((String) map.get("downloadUrl"));
			userLoginVo.setSign("appSPOS_userLogin_sign");
			return userLoginVo;
		} catch (Exception e) {
			logger.error(this, e);
			userLoginVo.setResp_status("9999");
			userLoginVo.setMsg("调用接口失败");
			return userLoginVo;
		}
	}

	/**
	 * 登出
	 */
	@Override
	public UserLogoutVo userLogout(String token) {
		UserLogoutVo userLogoutVo = new UserLogoutVo();
		try {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("token", token);
			if (logger.isInfoEnabled()) {
				logger.info("调帐户系统用户注销:" + params);
			}
			Map<String, Object> map = HttpClient.getInstance()
					.doPOSTgetHashMap("/accmanage/user_logout", params);
			if (logger.isInfoEnabled()) {
				logger.info("帐户系统用户注销返回:" + map);
			}
			if (map == null) {
				userLogoutVo.setResp_status("9999");
				userLogoutVo.setMsg("帐户系统用户注销返回空");
				return userLogoutVo;
			}
			String[] resTip = ResourceMap.getArray("A_user_logout_" + (String) map.get("status"));
			userLogoutVo.setResp_status(resTip[0]);
			userLogoutVo.setMsg(resTip[1]);
			userLogoutVo.setSign("appSPOS_userLogout_sign");
			return userLogoutVo;
		} catch (Exception e) {
			logger.error(this, e);
			return userLogoutVo;
		}
	}

	/**
	 * 登陆第二步，申请发送手机验证码验证码
	 */
	@Override
	public CertiCodeResendVo certiCodeResend(String tokenLow) {
		CertiCodeResendVo certiCodeResendVo = new CertiCodeResendVo();
		try {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("token", tokenLow);
			if (logger.isInfoEnabled()) {
				logger.info("调帐户系统重发验证码:" + params);
			}
			Map<String, Object> map = HttpClient.getInstance()
					.doPOSTgetHashMap("/accmanage/reget_mobi_ph", params);
			if (logger.isInfoEnabled()) {
				logger.info("帐户系统重发验证码返回:" + map);
			}
			if(map == null) {
				certiCodeResendVo.setResp_status("9999");
				certiCodeResendVo.setMsg("帐户系统重发验证码返回空");
				return certiCodeResendVo;
			}
			String[] resTip = ResourceMap.getArray("A_reget_mobi_ph_" + (String) map.get("status"));
			certiCodeResendVo.setResp_status(resTip[0]);
			certiCodeResendVo.setMsg(resTip[1]);
			certiCodeResendVo.setToken_low((String) map.get("tmpToken"));
			certiCodeResendVo.setSign("appSPOS_certiCodeResend_sign");
			return certiCodeResendVo;
		} catch (Exception e) {
			logger.error(this, e);
			return certiCodeResendVo;
		}
	}

	/**
	 * 登陆第三步，验证短信码，身份校验请求
	 * @return 
	 */
	@Override
	public ValidateUserCertiVo validateUserCerti(String tokenLow, String certiCode) {
		ValidateUserCertiVo validateUserCertiVo = new ValidateUserCertiVo();
		try {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("tmpToken", tokenLow);
			params.put("smsCode", certiCode);
			if (logger.isInfoEnabled()) {
				logger.info("调帐户系统校验身份:" + params);
			}
			Map<String, Object> map = HttpClient.getInstance()
					.doPOSTgetHashMap("/accmanage/user_login", params);
			if (logger.isInfoEnabled()) {
				logger.info("帐户系统校验身份返回:" + map);
			}
			if (map == null) {
				validateUserCertiVo.setResp_status("9999");
				validateUserCertiVo.setMsg("帐户系统校验身份返回空");
				return validateUserCertiVo;
			}
			String[] resTip = ResourceMap.getArray("A_user_login_" + (String) map.get("status"));
			validateUserCertiVo.setResp_status(resTip[0]);
			validateUserCertiVo.setMsg(resTip[1]);
			validateUserCertiVo.setToken((String)map.get("token"));
			validateUserCertiVo.setValid_time((String)map.get("tokenValidTime"));
			validateUserCertiVo.setGesture_password((String)map.get("gesturePwd"));
			validateUserCertiVo.setUser_name((String)map.get("username"));
			validateUserCertiVo.setUser_logo((String)map.get("userLogo"));
			validateUserCertiVo.setLastLoginTime((String)map.get("lastLoginTime"));
//			validateUserCertiVo.setUser_role("");//返回中未定义
//			validateUserCertiVo.setMerchant_logo("");//返回中未定义
//			validateUserCertiVo.setMerchant_name("");//返回中未定义
			validateUserCertiVo.setSign("appSPOS_validateUserCerti_sign");
			return validateUserCertiVo;
		} catch (Exception e) {
			logger.error(this, e);
			return validateUserCertiVo;
		}
	}

}
