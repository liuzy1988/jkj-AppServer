package com.jiankangjin.appserver.services.impl;

import org.springframework.stereotype.Service;

import com.jiankangjin.appserver.services.UserOrderServices;
import com.jiankangjin.appserver.vo.CreateOrderVo;
import com.jiankangjin.appserver.vo.PayOrderVo;

/**
 * @author liuzy 
 * @date 2014年11月27日 下午1:39:41 
 */
@Service
public class UserOrderServicesImpl implements UserOrderServices {

	/**
	 * @param merchant_id
	 * @param user_id
	 * @param user_name
	 * @param merchant_order_id
	 * @param _total_fee
	 * @param currency
	 * @param content
	 * @param detail
	 * @param _is_tip
	 * @param buyer_name
	 * @param buyer_cellphone
	 * @param comment
	 * @param order_total_fee
	 * @param discount_fee
	 * @param discount_comment
	 * @return
	 */
	@Override
	public CreateOrderVo createOrder(String merchant_id, String user_id,
			String user_name, String merchant_order_id, Integer _total_fee,
			String currency, String content, String detail, boolean _is_tip,
			String buyer_name, String buyer_cellphone, String comment,
			String order_total_fee, String discount_fee, String discount_comment) {
		return null;
	}

	/**
	 * @param token
	 * @param user_id
	 * @param user_name
	 * @param local_pay_ssn
	 * @param order_id
	 * @param _fee
	 * @param currency
	 * @param channel_id
	 * @param buyer_token
	 * @param buyer_voucher
	 * @param channel_type
	 * @return
	 */
	@Override
	public PayOrderVo payOrder(String token, String user_id, String user_name,
			String local_pay_ssn, String order_id, Integer _fee,
			String currency, String channel_id, String buyer_token,
			String buyer_voucher, String channel_type) {
		return null;
	}

}
