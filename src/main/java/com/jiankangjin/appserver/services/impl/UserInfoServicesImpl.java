package com.jiankangjin.appserver.services.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.jiankangjin.appserver.services.UserInfoServerices;
import com.jiankangjin.appserver.util.ResourceMap;
import com.jiankangjin.appserver.vo.GetUserInfoVo;
import com.jiankangjin.appserver.vo.UpdPasswordVo;
import com.jiankangjin.httpclient.HttpClient;

/**
 * @author liuzy 
 * @date 2014年11月27日 上午11:00:29 
 */
@Service
public class UserInfoServicesImpl implements UserInfoServerices {
	private static final transient Log logger = LogFactory.getLog(UserInfoServicesImpl.class);

	/**
	 * 下载用户信息
	 */
	@Override
	public GetUserInfoVo getUserInfo(String token) {
		GetUserInfoVo getUserInfoVo=new GetUserInfoVo();
		try {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("token", token);
			if (logger.isInfoEnabled()) {
				logger.info("调帐户系统下载用户信息:" + params);
			}
			Map<String, Object> map = HttpClient.getInstance()
					.doPOSTgetHashMap("/accmanage/query_user_info", params);
			if (logger.isInfoEnabled()) {
				logger.info("帐户系统下载用户信息返回:" + map);
			}
			if (map == null) {
				getUserInfoVo.setResp_status("9999");
				getUserInfoVo.setMsg("帐户系统下载用户信息返回空");
				return getUserInfoVo;
			}
			String[] resTip = ResourceMap.getArray("A_query_user_info_" + (String) map.get("status"));
			getUserInfoVo.setResp_status(resTip[0]);
			getUserInfoVo.setMsg(resTip[1]);
			getUserInfoVo.setToken((String) map.get("token"));
			getUserInfoVo.setUser_name((String) map.get("username"));
            getUserInfoVo.setUuid((String)map.get("userid"));
			getUserInfoVo.setUser_gender((String) map.get("gender"));
			getUserInfoVo.setUser_addr((String) map.get("address"));
			getUserInfoVo.setSign("appSPOS_getUserInfo_sign");
			return getUserInfoVo;
		} catch (Exception e) {
			logger.error(this, e);
			return getUserInfoVo;
		}
	}




	/**
	 * 密码更新
	 */
	@Override
	public UpdPasswordVo updPassword(String token, String oldPassword, String newPassword) {
		UpdPasswordVo updPasswordVo = new UpdPasswordVo();
		try {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("token", token);
			params.put("oldPwd", oldPassword);
			params.put("newPwd", newPassword);
			if (logger.isInfoEnabled()) {
				logger.info("调帐户系统更新密码:" + params);
			}
			Map<String, Object> map = HttpClient.getInstance()
					.doPOSTgetHashMap("/accmanage/change_pwd", params);
			if (logger.isInfoEnabled()) {
				logger.info("帐户系统更新密码返回:" + map);
			}
			if(map == null) {
				updPasswordVo.setResp_status("9999");
				updPasswordVo.setMsg("调帐户系统更新密码空");
				return updPasswordVo;
			}
			String[] resTip = ResourceMap.getArray("A_change_pwd_" + (String) map.get("status"));
			updPasswordVo.setResp_status(resTip[0]);
			updPasswordVo.setMsg(resTip[1]);	
			updPasswordVo.setSign("appSPOS_updPassword_sign");
			return updPasswordVo;
		} catch (Exception e) {
			logger.error(this, e);
			return updPasswordVo;
		}
	}


}
