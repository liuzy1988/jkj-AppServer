<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head lang="zh-CN">
<meta http-equiv="content-type" content="text/html;charset=utf-8">
<title>AppServer接口测试页面</title>
<head>
</head>
<body bgcolor="#336699">
	<br>
	<h1 align="center">AppServer接口测试页面<a href="./res/index.html">转到Ajax版测试</a></h1>
	<div id="top">
		<table cellspacing="0" cellpadding="0" align="center" width="80%">
			<tr>
				<td width="20%" align="center">
					<input type="button" id="bt1" name="bt1" value="3. 用户动作相关接口" onclick="bt1_click()" style="border:0px solid #613030; width: 100%; font-size:14pt;">
				</td>
				<td width="20%" align="center">
					<input type="button" id="bt2" name="bt2" value="4. 用户信息相关接口" onclick="bt2_click()" style="border:0px solid #616130; width: 100%; font-size:14pt;">
				</td>
				<td width="20%" align="center">
					<input type="button" id="bt3" name="bt3" value="5. 用户账户相关接口" onclick="bt3_click()" style="border:0px solid #336666; width: 100%; font-size:14pt;">
				</td>
				<td width="20%" align="center">
					<input type="button" id="bt4" name="bt4" value="6. 用户账单相关接口" onclick="bt4_click()" style="border:0px solid #484891; width: 100%; font-size:14pt;">
				</td>
				<td width="20%" align="center">
					<input type="button" id="bt5" name="bt5" value="7. 其他功能接口" onclick="bt5_click()"" style="border:0px solid #6C3365; width: 100%; font-size:14pt;">
				</td>
			</tr>
		</table>
	</div>
	<!-- 3. 用户动作相关接口 -->
	<div id="div1" style="width:80%; border:0px solid #613030; display:block; background-color:#E0E0E0; margin-left: auto; margin-right: auto">
		<br />
		<fieldset style="width: 600px; margin: auto">
			<legend>3.1. 校验账户是否存在 check_user_exists</legend>
			<form action="${pageContext.request.contextPath}/check_user_exists"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>token：</td>
						<td><input type="text" size="40" name="token"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>user_name：</td>
						<td><input type="text" size="40" name="user_name"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>user_type：</td>
						<td><input type="text" size="40" name="user_type"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>sign：</td>
						<td><input type="text" size="40" name="sign"
							value="AppServer_Web_Testing"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br />
		<fieldset style="width: 600px; margin: auto">
			<legend>3.2. 用户注册 user_register</legend>
			<form action="${pageContext.request.contextPath}/user_register"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>token：</td>
						<td><input type="text" size="40" name="token"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>user_name：</td>
						<td><input type="text" size="40" name="user_name"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>user_type：</td>
						<td><input type="text" size="40" name="user_type"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>sign：</td>
						<td><input type="text" size="40" name="sign"
							value="AppServer_Web_Testing"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br />
		<fieldset style="width: 600px; margin: auto">
			<legend>3.3. 更新验证码 update_validate_img</legend>
			<form action="${pageContext.request.contextPath}/update_validate_img"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>无参数：</td>
						<td><input type="text" size="40" name="" placeholder="无参数"
							readonly="readonly"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br />
		<fieldset style="width: 600px; margin: auto">
		<legend>3.4. 用户登陆 user_login</legend>
		<form action="${pageContext.request.contextPath}/user_login"
			method="post" accept-charset="utf-8">
			<table cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td>验证会话编号：</td>
					<td><input type="text" size="40" name="tmp_ssn"
						placeholder="请输入"></td>
				</tr>
				<tr>
					<td>验证码：</td>
					<td><input type="text" size="40" name="validate_code"
						placeholder="请输入"></td>
				</tr>
				<tr>
					<td>登录账号：</td>
					<td><input type="text" size="40" name="login_voucher"
						placeholder="请输入" value="13162165337"></td>
				</tr>
				<tr>
					<td>登录密码：</td>
					<td><input type="text" size="40" name="login_password"
						placeholder="请输入" value="123456"></td>
				</tr>
				<tr>
					<td>终端类型：</td>
					<td><select name="terminal_type">
							<option value="">=请选择终端类型=</option>
							<option value="android-app" selected="selected">安卓手机</option>
							<option value="android-pad">安卓Pad</option>
							<option value="wp-app">WP手机</option>
							<option value="wp-pad">WP平板</option>
							<option value="iOS">iOS手机</option>
							<option value="iPad">iPad</option></td>
				</tr>
				<tr>
					<td>终端软件版本：</td>
					<td><input type="text" size="40" name="terminal_ver"
						placeholder="请输入当前终端版本号" value="v1.9.9"></td>
				</tr>
				<tr>
					<td>终端标识：</td>
					<td><input type="text" size="40" name="terminal_id"
						placeholder="请输入终端唯一标识号"></td>
				</tr>
				<tr>
					<td>签名：</td>
					<td><input type="text" size="40" name="sign"
						value="AppServer_Web_Testing"></td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" value="提 交"></td>
				</tr>
			</table>
		</form>
	</fieldset>
	<br>
	<fieldset style="width: 600px; margin: auto">
		<legend>3.5. 身份校验凭证重发 certi_code_resend</legend>
		<form action="${pageContext.request.contextPath}/certi_code_resend"
			method="post" accept-charset="utf-8">
			<table cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td>低权限访问令牌：</td>
					<td><input type="text" size="40" name="token_low"
						placeholder="请输入"></td>
				</tr>
				<tr>
					<td>签名：</td>
					<td><input type="text" size="40" name="sign"
						value="AppServer_Web_Testing"></td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" value="提 交"></td>
				</tr>
			</table>
		</form>
	</fieldset>
	<br>
	<fieldset style="width: 600px; margin: auto">
		<legend>3.6. 身份校验(短信码)请求  validate_user_certi</legend>
		<form action="${pageContext.request.contextPath}/validate_user_certi"
			method="post" accept-charset="utf-8">
			<table cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td>低权限访问令牌：</td>
					<td><input type="text" size="40" name="token_low"
						placeholder="请输入"></td>
				</tr>
				<tr>
					<td>身份校验码：</td>
					<td><input type="text" size="40" name="certi_code"
						placeholder="请输入"></td>
				</tr>
				<tr>
					<td>签名：</td>
					<td><input type="text" size="40" name="sign"
						value="AppServer_Web_Testing"></td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" value="提 交"></td>
				</tr>
			</table>
		</form>
	</fieldset>
	<br>
	<fieldset style="width: 600px; margin: auto">
		<legend>3.7. 用户注销登陆 user_logout</legend>
		<form action="${pageContext.request.contextPath}/user_logout"
			method="post" accept-charset="utf-8">
			<table cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td>访问令牌 ：</td>
					<td><input type="text" size="40" name="token"
						value=""></td>
				</tr>
				<tr>
					<td>签名：</td>
					<td><input type="text" size="40" name="sign"
						value="AppServer_Web_Testing"></td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" value="提 交"></td>
				</tr>
			</table>
		</form>
	</fieldset>
	<br>
	</div>
	<!-- 4. 用户信息相关接口 -->
	<div id="div2" style="width:80%; border:0px solid #616130; display:none; background-color:#E0E0E0; margin-left: auto; margin-right: auto">
		<br>
		<fieldset style="width: 600px; margin: auto">
			<legend>4.1. 实名认证 validate_real_name</legend>
			<form action="${pageContext.request.contextPath}/validate_real_name"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>token：</td>
						<td><input type="text" size="40" name="token" placeholder="请输入"></td>
					</tr>
					<tr>
						<td>sign：</td>
						<td><input type="text" size="40" name="sign"
							value="AppServer_Web_Testing"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br>
		<fieldset style="width: 600px; margin: auto">
			<legend>4.2. 获取实名认证协议 get_real_name_agreement</legend>
			<form action="${pageContext.request.contextPath}/get_real_name_agreement"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>token：</td>
						<td><input type="text" size="40" name="token" placeholder="请输入"></td>
					</tr>
					<tr>
						<td>sign：</td>
						<td><input type="text" size="40" name="sign"
							value="AppServer_Web_Testing"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br>
		<fieldset style="width: 600px; margin: auto">
			<legend>4.3. 验证实名信息 validate_real_name_info</legend>
			<form action="${pageContext.request.contextPath}/validate_real_name_info"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>token：</td>
						<td><input type="text" size="40" name="token" placeholder="请输入"></td>
					</tr>
					<tr>
						<td>sign：</td>
						<td><input type="text" size="40" name="sign"
							value="AppServer_Web_Testing"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br>
		<fieldset style="width: 600px; margin: auto">
			<legend>4.4. 设置用户密保问题 upd_security_question</legend>
			<form action="${pageContext.request.contextPath}/upd_security_question"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>token：</td>
						<td><input type="text" size="40" name="token" placeholder="请输入"></td>
					</tr>
					<tr>
						<td>sign：</td>
						<td><input type="text" size="40" name="sign"
							value="AppServer_Web_Testing"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br>
		<fieldset style="width: 600px; margin: auto">
			<legend>4.5. 设置用户头像 upd_user_head_photo</legend>
			<form action="${pageContext.request.contextPath}/upd_user_head_photo"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>token：</td>
						<td><input type="text" size="40" name="token" placeholder="请输入"></td>
					</tr>
					<tr>
						<td>sign：</td>
						<td><input type="text" size="40" name="sign"
							value="AppServer_Web_Testing"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br>
		<fieldset style="width: 600px; margin: auto">
			<legend>4.6. 下载用户信息 get_user_info</legend>
			<form action="${pageContext.request.contextPath}/get_user_info"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>访问令牌 ：</td>
						<td><input type="text" size="40" name="token"
							value=""></td>
					</tr>
					<tr>
						<td>签名：</td>
						<td><input type="text" size="40" name="sign"
							value="AppServer_Web_Testing"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br>
		<fieldset style="width: 600px; margin: auto">
			<legend>4.7. 修改登陆密码 upd_password</legend>
			<form action="${pageContext.request.contextPath}/upd_password"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>访问令牌 ：</td>
						<td><input type="text" size="40" name="token"
							value=""></td>
					</tr>
					<tr>
						<td>当前密码 ：</td>
						<td><input type="text" size="40" name="old_password"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>修改后密码 ：</td>
						<td><input type="text" size="40" name="new_password"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>签名：</td>
						<td><input type="text" size="40" name="sign"
							value="AppServer_Web_Testing"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br>
		<fieldset style="width: 600px; margin: auto">
			<legend>4.8. 修改支付密码 upd_pay_password</legend>
			<form action="${pageContext.request.contextPath}/upd_pay_password"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>token：</td>
						<td><input type="text" size="40" name="token" placeholder="请输入"></td>
					</tr>
					<tr>
						<td>sign：</td>
						<td><input type="text" size="40" name="sign"
							value="AppServer_Web_Testing"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br>
		<fieldset style="width: 600px; margin: auto">
			<legend>4.9. 验证支付密码 validate_pay_password</legend>
			<form action="${pageContext.request.contextPath}/validate_pay_password"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>token：</td>
						<td><input type="text" size="40" name="token" placeholder="请输入"></td>
					</tr>
					<tr>
						<td>sign：</td>
						<td><input type="text" size="40" name="sign"
							value="AppServer_Web_Testing"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br>
	</div>
	<!-- 5. 用户账户相关接口 -->
	<div id="div3" style="width:80%; border:0px solid #336666; display:none; background-color:#E0E0E0; margin-left: auto; margin-right: auto">
		<br />
		<fieldset style="width: 600px; margin: auto">
			<legend>5.1. 用户绑定账户/卡 bind_user_account</legend>
			<form action="${pageContext.request.contextPath}/bind_user_account"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>token：</td>
						<td><input type="text" size="40" name="token"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>card_id：</td>
						<td><input type="text" size="40" name="card_id"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>card_type：</td>
						<td><input type="text" size="40" name="card_type"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>user_name：</td>
						<td><input type="text" size="40" name="user_name"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>id_card：</td>
						<td><input type="text" size="40" name="id_card"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>phone_number：</td>
						<td><input type="text" size="40" name="phone_number"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>card_password：</td>
						<td><input type="text" size="40" name="card_password"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>sign：</td>
						<td><input type="text" size="40" name="sign"
							value="AppServer_Web_Testing"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br />
		<fieldset style="width: 600px; margin: auto">
			<legend>5.2. 用户已绑定卡列表查询 all_user_account</legend>
			<form action="${pageContext.request.contextPath}/all_user_account"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>token：</td>
						<td><input type="text" size="40" name="token"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>card_id：</td>
						<td><input type="text" size="40" name="card_id"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>card_type：</td>
						<td><input type="text" size="40" name="card_type"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>sign：</td>
						<td><input type="text" size="40" name="sign"
							value="AppServer_Web_Testing"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br />
		<fieldset style="width: 600px; margin: auto">
			<legend>5.3. 用户解绑账户/卡 unbind_user_account</legend>
			<form action="${pageContext.request.contextPath}/unbind_user_account"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>token：</td>
						<td><input type="text" size="40" name="token"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>card_id：</td>
						<td><input type="text" size="40" name="card_id"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>card_type：</td>
						<td><input type="text" size="40" name="card_type"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>sign：</td>
						<td><input type="text" size="40" name="sign"
							value="AppServer_Web_Testing"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br />
		<fieldset style="width: 600px; margin: auto">
			<legend>5.4. 卡信息查询 get_account_info</legend>
			<form action="${pageContext.request.contextPath}/get_account_info"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>token：</td>
						<td><input type="text" size="40" name="token"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>card_id：</td>
						<td><input type="text" size="40" name="card_id"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>card_type：</td>
						<td><input type="text" size="40" name="card_type"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>sign：</td>
						<td><input type="text" size="40" name="sign"
							value="AppServer_Web_Testing"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br />
		<fieldset style="width: 600px; margin: auto">
			<legend>5.5. 查询卡消费记录 get_account_bill</legend>
			<form action="${pageContext.request.contextPath}/get_account_bill"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>token：</td>
						<td><input type="text" size="40" name="token"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>card_id：</td>
						<td><input type="text" size="40" name="card_id"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>card_type：</td>
						<td><input type="text" size="40" name="card_type"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>sign：</td>
						<td><input type="text" size="40" name="sign"
							value="AppServer_Web_Testing"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br />
		<fieldset style="width: 600px; margin: auto">
			<legend>5.6. 授权申请 apply_permission</legend>
			<form action="${pageContext.request.contextPath}/apply_permission"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>token：</td>
						<td><input type="text" size="40" name="token"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>to_user_id：</td>
						<td><input type="text" size="40" name="to_user_id"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>to_user_name：</td>
						<td><input type="text" size="40" name="to_user_name"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>message：</td>
						<td><input type="text" size="40" name="message"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>sign：</td>
						<td><input type="text" size="40" name="sign"
							value="AppServer_Web_Testing"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br />
		<fieldset style="width: 600px; margin: auto">
			<legend>5.7. 授权批复 reply_applied_permission</legend>
			<form action="${pageContext.request.contextPath}/reply_applied_permission"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>token：</td>
						<td><input type="text" size="40" name="token"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>to_user_id：</td>
						<td><input type="text" size="40" name="to_user_id"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>reply：</td>
						<td><input type="text" size="40" name="reply"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>sign：</td>
						<td><input type="text" size="40" name="sign"
							value="AppServer_Web_Testing"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br />
		<fieldset style="width: 600px; margin: auto">
			<legend>5.8. 查询授权列表 all_user_permission</legend>
			<form action="${pageContext.request.contextPath}/all_user_permission"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>token：</td>
						<td><input type="text" size="40" name="token"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>sign：</td>
						<td><input type="text" size="40" name="sign"
							value="AppServer_Web_Testing"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br/>
	</div>
	<!-- 6. 用户账单相关接口 -->
	<div id="div4" style="width:80%; border:0px solid #484891; display:none; background-color:#E0E0E0; margin-left: auto; margin-right: auto">
		<br />
		<fieldset style="width: 600px; margin: auto">
			<legend>6.1. 创建订单 create_order</legend>
			<form action="${pageContext.request.contextPath}/create_order"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>访问令牌 ：</td>
						<td><input type="text" size="40" name="token"
							placeholder="正式访问令牌"></td>
					</tr>
					<tr>
						<td>版本 ：</td>
						<td><input type="text" size="40" name="version"
							value="v0.0.2"></td>
					</tr>
					<tr>
						<td>商户编号 ：</td>
						<td><input type="text" size="40" name="merchant_id"
							value="206064184488"></td>
					</tr>
					<tr>
						<td>商户本地订单编号 ：</td>
						<td><input type="text" size="40" name="merchant_order_id"
							placeholder="请输入"></td>
					</tr>
					<tr>
						<td>订单实际金额 ：</td>
						<td><input type="text" size="40" name="order_total_fee"
							placeholder="请输入" value="1999"></td>
					</tr>
					<tr>
						<td>折扣金额 ：</td>
						<td><input type="text" size="40" name="discount_fee"
							placeholder="请输入" value="999"></td>
					</tr>
					<tr>
						<td>折扣原因 ：</td>
						<td><input type="text" size="40" name="discount_comment"
							placeholder="请输入" value="双十一甩卖"></td>
					</tr>
					<tr>
						<td>订单金额 ：</td>
						<td><input type="text" size="40" name="total_fee"
							placeholder="请输入" value="1000"></td>
					</tr>
					<tr>
						<td>订单币种 ：</td>
						<td><input type="text" size="40" name="currency"
							placeholder="默认为156，即人民币" value="156"></td>
					</tr>
					<tr>
						<td>订单概要 ：</td>
						<td><input type="text" size="40" name="content"
							placeholder="请输入" value="专治各种不服"></td>
					</tr>
					<tr>
						<td>订单详情 ：</td>
						<td><textarea name="detail" placeholder="Array:Goods"
								rows="5" cols="45"></textarea></td>
					</tr>
					<tr>
						<td>提醒补全 ：</td>
						<td><input type="text" size="40" name="is_tip"
							placeholder="true" value="true"></td>
					</tr>
					<tr>
						<td>购买者姓名 ：</td>
						<td><input type="text" size="40" name="buyer_name"
							placeholder="请输入" value="小田田"></td>
					</tr>
					<tr>
						<td>购买者手机 ：</td>
						<td><input type="text" size="40" name="buyer_cellphone"
							placeholder="请输入" value="13952526202"></td>
					</tr>
					<tr>
						<td>注释 ：</td>
						<td><input type="text" size="40" name="comment"
							placeholder="请输入" value="上海各大药房有售"></td>
					</tr>
					<tr>
						<td>签名：</td>
						<td><input type="text" size="40" name="sign"
							placeholder="请输入" value="测试-网页-PAD-签名"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br/>
		<fieldset style="width: 600px; margin: auto">
			<legend>6.2. 订单支付    pay_order</legend>
			<form action="${pageContext.request.contextPath}/pay_order"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>访问令牌 ：</td>
						<td><input type="text" size="40" name="token" placeholder="正式访问令牌"></td>
					</tr>
					<tr>
						<td>本地支付流水号 ：</td>
						<td><input type="text" size="40" name="local_pay_ssn" placeholder="请输入"></td>
					</tr>
					<tr>
						<td>订单编号 ：</td>
						<td><input type="text" size="40" name="order_id" placeholder="请输入"></td>
					</tr>
					<tr>
						<td>支付金额 ：</td>
						<td><input type="text" size="40" name="fee" placeholder="请输入" value="1000"></td>
					</tr>
					<tr>
						<td>订单币种  ：</td>
						<td><input type="text" size="40" name="currency" placeholder="默认为156，即人民币" value="156"></td>
					</tr>
					<tr>
						<td>支付渠道编号  ：</td>
						<td><input type="text" size="40" name="channel_id" placeholder="请输入" value="0001"></td>
					</tr>
					<tr>
						<td>支付者令牌  ：</td>
						<td><input type="text" size="40" name="buyer_token" placeholder="扫码结果"></td>
					</tr>
					<tr>
						<td>支付者凭证  ：</td>
						<td><input type="text" size="40" name="buyer_voucher" placeholder="非空任意字符串"></td>
					</tr>
					<tr>
						<td>支付渠道类型  ：</td>
						<td><input type="text" size="40" name="channel_type" placeholder="请输入" value="0001"></td>
					</tr>
					<tr>
						<td>签名：</td>
						<td><input type="text" size="40" name="sign" value="测试-网页-PAD-签名"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br/>
		<fieldset style="width: 600px; margin: auto">
			<legend>6.3. 订单查询 search_order</legend>
			<form action="${pageContext.request.contextPath}/search_order"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>访问令牌 ：</td>
						<td><input type="text" size="40" name="token" placeholder="正式访问令牌"></td>
					</tr>
					<tr>
						<td>订单号 ：</td>
						<td><input type="text" size="40" name="order_id" placeholder="请输入"></td>
					</tr>
					<tr>
						<td>订单状态 ：</td>
						<td><input type="text" size="40" name="order_stat" placeholder="已申请退款的输入：0012"></td>
					</tr>
					<tr>
						<td>交易时间</td>
						<td><input type="text" size="40" name="order_date_begin" placeholder="请输入" ></td>
					</tr>
					<tr>
						<td>交易截止时间</td>
						<td><input type="text" size="40" name="order_date_end" placeholder="请输入"></td>
					</tr>
					<tr>
						<td>退款时间</td>
						<td><input type="text" size="40" name="refund_date_begin" placeholder="请输入" ></td>
					</tr>
					<tr>
						<td>退款截止时间</td>
						<td><input type="text" size="40" name="refund_date_end" placeholder="请输入"></td>
					</tr>
					<tr>
						<td>检索信息</td>
						<td><input type="text" size="40" name="search_word" placeholder="请输入"></td>
					</tr>
					<tr>
						<td>签名：</td>
						<td><input type="text" size="40" name="sign" value="测试-网页-PAD-签名"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br>
		<fieldset style="width: 600px; margin: auto">
			<legend>6.4. 订单详情查询 get_order_result</legend>
			<form action="${pageContext.request.contextPath}/get_order_result"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>访问令牌 ：</td>
						<td><input type="text" size="40" name="token" placeholder="正式访问令牌"></td>
					</tr>
					<tr>
						<td>搜索结果编号 ：</td>
						<td><input type="text" size="40" name="result_id" placeholder="请输入"></td>
					</tr>
					<tr>
						<td>查询页码 ：</td>
						<td><input type="text" size="40" name="page" placeholder="1"></td>
					</tr>
					<tr>
						<td>查询记录条数 ：</td>
						<td><input type="text" size="40" name="quantity" placeholder="10"></td>
					</tr>
					<tr>
						<td>签名 ：</td>
						<td><input type="text" size="40" name="sign" value="测试-网页-PAD-签名"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br/>
		<fieldset style="width: 600px; margin: auto">
			<legend>6.5. 个人历月账单查询 search_all_month_bill</legend>
			<form action="${pageContext.request.contextPath}/search_all_month_bill"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>token ：</td>
						<td><input type="text" size="40" name="token" placeholder="正式访问令牌"></td>
					</tr>
					<tr>
						<td>date：</td>
						<td><input type="text" size="40" name="date" placeholder="时间"></td>
					</tr>
					<tr>
						<td>sign：</td>
						<td><input type="text" size="40" name="sign" value="测试-网页-PAD-签名"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br/>
		<fieldset style="width: 600px; margin: auto">
			<legend>6.6. 个人总账单查询 search_bill</legend>
			<form action="${pageContext.request.contextPath}/search_bill"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>token ：</td>
						<td><input type="text" size="40" name="token" placeholder="正式访问令牌"></td>
					</tr>
					<tr>
						<td>date：</td>
						<td><input type="text" size="40" name="date" placeholder="时间"></td>
					</tr>
					<tr>
						<td>sign：</td>
						<td><input type="text" size="40" name="sign" value="测试-网页-PAD-签名"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br/>
		<fieldset style="width: 600px; margin: auto">
			<legend>6.7. 个人对月账单查询 search_month_bill</legend>
			<form action="${pageContext.request.contextPath}/search_month_bill"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>token ：</td>
						<td><input type="text" size="40" name="token" placeholder="正式访问令牌"></td>
					</tr>
					<tr>
						<td>date：</td>
						<td><input type="text" size="40" name="date" placeholder="时间"></td>
					</tr>
					<tr>
						<td>other_user_id：</td>
						<td><input type="text" size="40" name="other_user_id" placeholder="时间"></td>
					</tr>
					<tr>
						<td>sign：</td>
						<td><input type="text" size="40" name="sign" value="测试-网页-PAD-签名"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br/>
		<fieldset style="width: 600px; margin: auto">
			<legend>6.8. 个人对月账单查询结果 get_month_bill_result</legend>
			<form action="${pageContext.request.contextPath}/get_month_bill_result"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>token ：</td>
						<td><input type="text" size="40" name="token" placeholder="正式访问令牌"></td>
					</tr>
					<tr>
						<td>result_id：</td>
						<td><input type="text" size="40" name="result_id" placeholder="时间"></td>
					</tr>
					<tr>
						<td>page：</td>
						<td><input type="text" size="40" name="page" placeholder="时间"></td>
					</tr>
					<tr>
						<td>quantiy：</td>
						<td><input type="text" size="40" name="quantiy" placeholder="时间"></td>
					</tr>
					<tr>
						<td>other_user_id：</td>
						<td><input type="text" size="40" name="other_user_id" placeholder="时间"></td>
					</tr>
					<tr>
						<td>sign：</td>
						<td><input type="text" size="40" name="sign" value="测试-网页-PAD-签名"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
	</div>
	<!-- 7. 其他功能接口 -->
	<div id="div5" style="width:80%; border:0px solid #6C3365; display:none; background-color:#E0E0E0; margin-left: auto; margin-right: auto">
		<br/>
		<fieldset style="width: 600px; margin: auto">
			<legend>7.1. 资讯列表查询 search_news</legend>
			<form action="${pageContext.request.contextPath}/search_news"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>访问令牌 ：</td>
						<td><input type="text" size="40" name="token" placeholder="正式访问令牌"></td>
					</tr>
					<tr>
						<td>签名：</td>
						<td><input type="text" size="40" name="sign" value="测试-网页-PAD-签名"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br/>
		<fieldset style="width: 600px; margin: auto">
			<legend>7.2. 资讯详情查询 get_news_result</legend>
			<form action="${pageContext.request.contextPath}/get_news_result"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>访问令牌 ：</td>
						<td><input type="text" size="40" name="token" placeholder="正式访问令牌"></td>
					</tr>
					<tr>
						<td>签名：</td>
						<td><input type="text" size="40" name="sign" value="测试-网页-PAD-签名"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br/>
		<fieldset style="width: 600px; margin: auto">
			<legend>7.3. 附近商户 near_merchants</legend>
			<form action="${pageContext.request.contextPath}/near_merchants"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>访问令牌 ：</td>
						<td><input type="text" size="40" name="token" placeholder="正式访问令牌"></td>
					</tr>
					<tr>
						<td>签名：</td>
						<td><input type="text" size="40" name="sign" value="测试-网页-PAD-签名"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br/>
		<fieldset style="width: 600px; margin: auto">
			<legend>7.4. 收藏商户 favorites_merchants</legend>
			<form action="${pageContext.request.contextPath}/favorites_merchants"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>访问令牌 ：</td>
						<td><input type="text" size="40" name="token" placeholder="正式访问令牌"></td>
					</tr>
					<tr>
						<td>签名：</td>
						<td><input type="text" size="40" name="sign" value="测试-网页-PAD-签名"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br/>
		<fieldset style="width: 600px; margin: auto">
			<legend>7.5. 检查更新 check_update</legend>
			<form action="${pageContext.request.contextPath}/check_update"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>访问令牌 ：</td>
						<td><input type="text" size="40" name="token" placeholder="正式访问令牌"></td>
					</tr>
					<tr>
						<td>签名：</td>
						<td><input type="text" size="40" name="sign" value="测试-网页-PAD-签名"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br/>
		<fieldset style="width: 600px; margin: auto">
			<legend>7.6. 提交反馈意见 submit_suggestions</legend>
			<form action="${pageContext.request.contextPath}/submit_suggestions"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>访问令牌 ：</td>
						<td><input type="text" size="40" name="token" placeholder="正式访问令牌"></td>
					</tr>
					<tr>
						<td>签名：</td>
						<td><input type="text" size="40" name="sign" value="测试-网页-PAD-签名"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br/>
		<fieldset style="width: 600px; margin: auto">
			<legend>7.7. 查询反馈意见列表 search_suggestions</legend>
			<form action="${pageContext.request.contextPath}/search_suggestions"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>访问令牌 ：</td>
						<td><input type="text" size="40" name="token" placeholder="正式访问令牌"></td>
					</tr>
					<tr>
						<td>签名：</td>
						<td><input type="text" size="40" name="sign" value="测试-网页-PAD-签名"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br/>
		<fieldset style="width: 600px; margin: auto">
			<legend>7.8. 查询反馈意见详情 get_suggestions_result</legend>
			<form action="${pageContext.request.contextPath}/get_suggestions_result"
				method="post" accept-charset="utf-8">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>访问令牌 ：</td>
						<td><input type="text" size="40" name="token" placeholder="正式访问令牌"></td>
					</tr>
					<tr>
						<td>签名：</td>
						<td><input type="text" size="40" name="sign" value="测试-网页-PAD-签名"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="提 交"></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<br/>
	</div>
	<div id="botem" align="center" style="color:#9D9D9D"><hr><u><i>By liuzy</i></u></div>
</body>
<script type="text/javascript">
function bt1_click(){
	document.getElementById('div1').style.display = 'block';
	document.getElementById('div2').style.display = 'none';
	document.getElementById('div3').style.display = 'none';
	document.getElementById('div4').style.display = 'none';
	document.getElementById('div5').style.display = 'none';
}
function bt2_click(){
	document.getElementById('div1').style.display = 'none';
	document.getElementById('div2').style.display = 'block';
	document.getElementById('div3').style.display = 'none';
	document.getElementById('div4').style.display = 'none';
	document.getElementById('div5').style.display = 'none';
}
function bt3_click(){
	document.getElementById('div1').style.display = 'none';
	document.getElementById('div2').style.display = 'none';
	document.getElementById('div3').style.display = 'block';
	document.getElementById('div4').style.display = 'none';
	document.getElementById('div5').style.display = 'none';
}
function bt4_click(){
	document.getElementById('div1').style.display = 'none';
	document.getElementById('div2').style.display = 'none';
	document.getElementById('div3').style.display = 'none';
	document.getElementById('div4').style.display = 'block';
	document.getElementById('div5').style.display = 'none';
}
function bt5_click(){
	document.getElementById('div1').style.display = 'none';
	document.getElementById('div2').style.display = 'none';
	document.getElementById('div3').style.display = 'none';
	document.getElementById('div4').style.display = 'none';
	document.getElementById('div5').style.display = 'block';
}
</script>
</html>
